package com.esionet.entity;

import java.io.Serializable;

/**
 * @author chenxuan
 * @date 2018/4/26 9:08
 */
public class ColumnVo implements Serializable {
    private static final long serialVersionUID = 1L;
    //列名
    private String columnName;
    //列名备注
    private String comments;

    //属性转驼峰
    private String attrName;

    //最大长度
    private String maxLength;

    //文本提示
    private String placeholder;

    //默认值
    private String defaultValue;

    //验证类型
    private String layVerify;

    //显示类型  inlinr  normal  block
    private String displayType;

    //元素类型
    private String elementType;

    //数据源类型  url codeName  enumName;
    private String dataType;

    //数据源
    private String dataResource;

    private  String required;

    public String getColumnName() {
        return columnName;
    }

    public void setColumnName(String columnName) {
        this.columnName = columnName;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public String getAttrName() {
        return attrName;
    }

    public void setAttrName(String attrName) {
        this.attrName = attrName;
    }

    public String getMaxLength() {
        return maxLength;
    }

    public void setMaxLength(String maxLength) {
        this.maxLength = maxLength;
    }

    public String getPlaceholder() {
        return placeholder;
    }

    public void setPlaceholder(String placeholder) {
        this.placeholder = placeholder;
    }

    public String getDefaultValue() {
        return defaultValue;
    }

    public void setDefaultValue(String defaultValue) {
        this.defaultValue = defaultValue;
    }

    public String getLayVerify() {
        return layVerify;
    }

    public void setLayVerify(String layVerify) {
        this.layVerify = layVerify;
    }

    public String getDisplayType() {
        return displayType;
    }

    public void setDisplayType(String displayType) {
        this.displayType = displayType;
    }

    public String getDataType() {
        return dataType;
    }

    public void setDataType(String dataType) {
        this.dataType = dataType;
    }

    public String getDataResource() {
        return dataResource;
    }

    public void setDataResource(String dataResource) {
        this.dataResource = dataResource;
    }

    public String getRequired() {
        return required;
    }

    public void setRequired(String required) {
        this.required = required;
    }

    public String getElementType() {
        return elementType;
    }

    public void setElementType(String elementType) {
        this.elementType = elementType;
    }
}
