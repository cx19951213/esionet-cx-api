package com.esionet.entity;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;


/**
 * 参数
 * @author chenxuan
 * @date 2019-06-15 13:50:08
 */
@Getter
@Setter
public class SysConfig implements Serializable {
	private static final long serialVersionUID = 1L;
	
	/**主键**/
	private String id;
	/**code**/
	private String code;
	/**value**/
	private String value;
	/**状态  0：禁用   1：正常**/
	private String status;
	/**备注**/
	private String remark;
	/**是否系统内置**/
	private String isSys;
	/**创建时间**/
	private Date createTime;
	/**更新时间**/
	private Date updateTime;

}
