package com.esionet.entity;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;


/**
 * 文件
 * @author chenxuan
 * @date 2019-07-03 21:08:27
 */
@Getter
@Setter
public class SysFile implements Serializable {
	private static final long serialVersionUID = 1L;
	
	/**主键**/
	private String id;
	/**文件MD5**/
	private String md5;
	/**文件名**/
	private String fileName;
	/**文件地址**/
	private String fileUrl;
	/**上级文件夹ID**/
	private String parentId;
	/**是否是文件夹**/
	private String isFolder;
	/**文件类型**/
	private String fileType;
	/**文件大小(k)**/
	private long fileSize;
	/**文件后缀**/
	private String fileSuffix;
	/**是否删除**/
	private String isDel;
	/**创建时间**/
	private Date createTime;
	/**更新时间**/
	private Date updateTime;
	/**删除时间**/
	private Date deleteTime;

}
