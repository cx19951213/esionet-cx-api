package com.esionet.entity;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;

/**
 * 系统用户
 * 
 * @author chenxuan
 */
@Getter
@Setter
public class SysUser implements Serializable {
	private static final long serialVersionUID = 1L;
    /**用户id**/
    private String userId;
    /**用户名**/
    private String username;
    /**密码**/
    private String password;
    /**姓名**/
    private String nickName;
    /**头像**/
    private String headPic;
    /**邮箱**/
    private String email;
    /**手机号**/
    private String mobile;
    /**性别**/
    private String gender;
    /**qq**/
    private String qq;
    /**状态**/
    private String status;
    /**创建者ID**/
    private String createUserId;
    /**角色id**/
    private String roleId;
    private String[] roleIds;
    /**部门id**/
    private String deptId;
    private String deptName;
    /**是否系统内置**/
    private String isSys;
    /**创建时间**/
    private Date createTime;
    /**token**/
    private String token;
    /**token过期时间**/
    private Date expireTime;
}
