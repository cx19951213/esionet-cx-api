package com.esionet.entity.dto;

import lombok.Getter;
import lombok.Setter;

/**
 * @File: UserInfoDto
 * @Author: chenxuan
 * @Version: 1.0
 * @Date: 2019/11/7
 * @Modify:
 * @Description:
 */
@Getter
@Setter
public class WxLoginDto {
    private String code;
    private String nickName;
    private String avatarUrl;
    private Integer gender;
    private String province;
    private String city;
    private String address;
}
