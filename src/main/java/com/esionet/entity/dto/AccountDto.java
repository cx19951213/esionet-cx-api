package com.esionet.entity.dto;

import lombok.Getter;
import lombok.Setter;

/**
 * @File: UserInfoDto
 * @Author: chenxuan
 * @Version: 1.0
 * @Date: 2019/11/7
 * @Modify:
 * @Description:
 */
@Getter
@Setter
public class AccountDto {

    /****/
    private String id;
    /**登录名(open_id)**/
    private String loginName;
    /**真实姓名**/
    private String realName;
    /**身份证号码**/
    private String idCard;
    /**用户昵称**/
    private String nickName;
    /**用户头像**/
    private String avatar;
    /**手机号码**/
    private String phone;
    /**微信号**/
    private String wechatNo;
    /**详细地址**/
    private String address;
    /**省**/
    private String province;
    /**市**/
    private String city;
    /**性别**/
    private String gender;
    /**是否为商家**/
    private String isShop;
    /**店铺名称**/
    private String shopName;
    /**token**/
    private String token;
    /**status**/
    private String status;
}
