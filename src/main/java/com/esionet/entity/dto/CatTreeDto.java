package com.esionet.entity.dto;

import java.util.List;

/**
 * Created by 陈熠
 * 2017/7/12.
 */
public class CatTreeDto {
    private String catId;
    private String catName;
    private String catImg;
    private List<CatTreeDto> children;

    public CatTreeDto() {
    }

    public CatTreeDto(String catId, String catName, String catImg, List<CatTreeDto> children) {
        this.catId = catId;
        this.catName = catName;
        this.catImg = catImg;
        this.children = children;
    }

    public String getCatId() {
        return catId;
    }

    public void setCatId(String catId) {
        this.catId = catId;
    }

    public String getCatName() {
        return catName;
    }

    public void setCatName(String catName) {
        this.catName = catName;
    }

    public String getCatImg() {
        return catImg;
    }

    public void setCatImg(String catImg) {
        this.catImg = catImg;
    }

    public List<CatTreeDto> getChildren() {
        return children;
    }

    public void setChildren(List<CatTreeDto> children) {
        this.children = children;
    }
}
