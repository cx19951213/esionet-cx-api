package com.esionet.entity.dto;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Getter
@Setter
@Document(collection = "chat_friend")
public class ChatFriendDto {
    @Id
    private String id;
    private String userId;
    private String friendId;
    private String headPic;
    private String nickName;
    private long count;
    private String lastContent;
    private String lastTime;
}
