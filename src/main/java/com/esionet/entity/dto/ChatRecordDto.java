package com.esionet.entity.dto;

import com.esionet.netty.entity.UserEntity;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;
import java.util.Date;

/**
 * @File: ChatEntity
 * @Author: chenxuan
 * @Version: 1.0
 * @Date: 2020/3/9
 * @Modify:
 * @Description: 用户发送消息实体
 */
@Getter
@Setter
@Document(collection = "chat_record")
public class ChatRecordDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    private String id;
    private String type;
    private String content;
    private String createTime;
    private UserEntity sender;
    private UserEntity receiver;
    private String senderId;
    private String receiverId;
    private String isRead;
}
