package com.esionet.entity;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;
import java.util.Date;


/**
 * 用户信息
 * @author chenxuan
 * @date 2020-03-09 15:44:49
 */
@Getter
@Setter
@Document(collection = "account")
public class Account implements Serializable {
	private static final long serialVersionUID = 1L;
	
	/****/
	@Id
	private String id;
	/**登录名(open_id)**/
	private String loginName;
	/**真实姓名**/
	private String realName;
	/**身份证号码**/
	private String idCard;
	/**用户昵称**/
	private String nickName;
	/**用户头像**/
	private String avatar;
	/**手机号码**/
	private String phone;
	/**微信号**/
	private String wechatNo;
	/**详细地址**/
	private String address;
	/**省**/
	private String province;
	/**市**/
	private String city;
	/**性别**/
	private String gender;
	/**是否为商家**/
	private String isShop;
	/**店铺名称**/
	private String shopName;
	/**注册IP**/
	private String registerIp;
	/**最后一次登录时间**/
	private Date lastLoginTime;
	/**最后一次登录IP**/
	private String lastLoginIp;
	/**token**/
	private String token;
	/**状态**/
	private String status;
	/**排序**/
	private String sortNo;
	/**创建时间**/
	private Date createTime;
	/**更新时间**/
	private Date updateTime;

}
