package com.esionet.entity;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;
import java.util.Date;


/**
 * 小程序按钮配置
 * @author chenxuan
 * @date 2019-11-05 14:10:24
 */
@Getter
@Setter
@Document(collection = "buttonConfig")
public class ButtonConfig implements Serializable {
	private static final long serialVersionUID = 1L;
	
	/**主键**/
	@Id
	private String id;
	/**名称**/
	private String name;
	/**图标**/
	private String image;
	/**小程序跳转路径**/
	private String appUrl;
	/**公众号跳转路径**/
	private String publicUrl;
	/**类型/位置**/
	private String type;
	/**是否删除**/
	private String isDel;
	/**状态**/
	private String status;
	/**排序**/
	private String sortNo;
	/**创建时间**/
	private Date createTime;
	/**更新时间**/
	private Date updateTime;
	/**是否跳转**/
	private String isSkip;

}
