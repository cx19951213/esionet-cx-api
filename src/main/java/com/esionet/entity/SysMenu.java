package com.esionet.entity;

import java.io.Serializable;
import java.util.List;


/**
 * 菜单
 * @author chenxuan
 * @date 2019-09-26 14:37:29
 */
public class SysMenu implements Serializable {
	private static final long serialVersionUID = 1L;
	
	/**菜单id**/
	private String menuId;
	/**上级id**/
	private String parentId;
	private String parentName;
	/**菜单名称**/
	private String name;
	/**权限**/
	private String perms;
	/**类型**/
	private String type;
	/**图标**/
	private String icon;
	/**排序**/
	private String orderNum;
	/**路由地址**/
	private String route;
	/**状态**/
	private String status;

	private List<SysMenu> children;

	public String getMenuId() {
		return menuId;
	}

	public void setMenuId(String menuId) {
		this.menuId = menuId;
	}

	public String getParentId() {
		return parentId;
	}

	public void setParentId(String parentId) {
		this.parentId = parentId;
	}

	public String getParentName() {
		return parentName;
	}

	public void setParentName(String parentName) {
		this.parentName = parentName;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPerms() {
		return perms;
	}

	public void setPerms(String perms) {
		this.perms = perms;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}

	public String getOrderNum() {
		return orderNum;
	}

	public void setOrderNum(String orderNum) {
		this.orderNum = orderNum;
	}

	public String getRoute() {
		return route;
	}

	public void setRoute(String route) {
		this.route = route;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public List<SysMenu> getChildren() {
		return children;
	}

	public void setChildren(List<SysMenu> children) {
		this.children = children;
	}
}
