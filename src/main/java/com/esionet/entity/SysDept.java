package com.esionet.entity;

import lombok.Getter;
import lombok.Setter;
import java.io.Serializable;
import java.util.Date;


/**
 * 部门
 * @author chenxuan
 * @date 2019-10-12 22:13:52
 */
@Getter
@Setter
public class SysDept implements Serializable {
	private static final long serialVersionUID = 1L;
	
	/**主键**/
	private String id;
	/**部门编号**/
	private String deptCode;
	/**部门名称**/
	private String deptName;
	/**上级部门ID**/
	private String parentId;
	private String parentName;
	/**排序**/
	private String sortNo;
	/**备注**/
	private String remark;
	/**状态**/
	private String status;
	/**是否系统内置**/
	private String isSys;
	/**创建时间**/
	private Date createTime;
	/**更新时间**/
	private Date updateTime;

}
