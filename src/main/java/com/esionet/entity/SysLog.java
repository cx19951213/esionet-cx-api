package com.esionet.entity;

import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;


/**
 * 操作日志
 * @author chenxuan
 * @date 2019-06-15 16:55:40
 */
@Getter
@Setter
public class SysLog implements Serializable {
	private static final long serialVersionUID = 1L;
	
	/**主键**/
	private String id;
	/**用户名**/
	private String username;
	/**操作**/
	private String operation;
	/**方法**/
	private String method;
	/**参数**/
	private String params;
	/**ip**/
	private String ip;
	/**备注**/
	private String remark;
	/**创建时间**/
	private Date createTime;

}
