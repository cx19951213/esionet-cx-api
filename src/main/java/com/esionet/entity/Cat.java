package com.esionet.entity;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;
import java.util.Date;


/**
 * 商品分类
 * @author chenxuan
 * @date 2020-03-08 15:17:20
 */
@Getter
@Setter
@Document(collection = "cat")
public class Cat implements Serializable {
	private static final long serialVersionUID = 1L;
	
	/**主键**/
	@Id
	private String id;
	/**分类名称**/
	private String catName;
	/**上级分类ID**/
	private String parentId;
	/**分类等级**/
	private String catLevel;
	/**分类图片**/
	private String catImg;
	/**是否推荐**/
	private String isRecommend;
	/**是否删除**/
	private String isDel;
	/**排序**/
	private String sortNo;
	/**创建时间**/
	private Date createTime;
	/**更新时间**/
	private Date updateTime;

}
