package com.esionet.entity;

import java.io.Serializable;
import java.util.Date;


/**
 * 字典管理
 * @author chenxuan
 * @date 2019-06-15 11:55:11
 */
public class SysDict implements Serializable {
	private static final long serialVersionUID = 1L;

	/**主键**/
	private String id;
	/**字典编码**/
	private String dictCode;
	/**字典值**/
	private String dictValue;
	/**字典名称**/
	private String dictName;
	/**排序**/
	private Integer sortNo;
	/**状态**/
	private String status;
	/**是否系统内置**/
	private String isSys;
	/**创建时间**/
	private Date createTime;
	/**更新时间**/
	private Date updateTime;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getDictCode() {
		return dictCode;
	}

	public void setDictCode(String dictCode) {
		this.dictCode = dictCode;
	}

	public String getDictValue() {
		return dictValue;
	}

	public void setDictValue(String dictValue) {
		this.dictValue = dictValue;
	}

	public String getDictName() {
		return dictName;
	}

	public void setDictName(String dictName) {
		this.dictName = dictName;
	}

	public Integer getSortNo() {
		return sortNo;
	}

	public void setSortNo(Integer sortNo) {
		this.sortNo = sortNo;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getIsSys() {
		return isSys;
	}

	public void setIsSys(String isSys) {
		this.isSys = isSys;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}
}
