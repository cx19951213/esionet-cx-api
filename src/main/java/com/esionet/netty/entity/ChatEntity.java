package com.esionet.netty.entity;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;
import java.util.Date;

/**
 * @File: ChatEntity
 * @Author: chenxuan
 * @Version: 1.0
 * @Date: 2020/3/9
 * @Modify:
 * @Description: 用户发送消息实体
 */
@Getter
@Setter
public class ChatEntity {

    private String type;
    private String content;
    private String createTime;
    private UserEntity sender;
    private UserEntity receiver;
}
