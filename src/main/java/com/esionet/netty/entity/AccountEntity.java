package com.esionet.netty.entity;

import lombok.Data;

import java.util.Date;

/**
 * @File: ChatEntity
 * @Author: chenxuan
 * @Version: 1.0
 * @Date: 2020/3/9
 * @Modify:
 * @Description: 用户登陆IM 实体
 */
@Data
public class AccountEntity {
    private String id;
    private String nickName;
    private String headPic;
    private Date createTime;


}
