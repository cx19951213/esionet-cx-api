package com.esionet.netty.entity;

import lombok.Data;

/**
 * @File: UserEntity
 * @Author: chenxuan
 * @Version: 1.0
 * @Date: 2020/3/10
 * @Modify:
 * @Description: 用户基本数据实体
 */
@Data
public class UserEntity {
    private String id;
    private String headPic;
    private String nickName;
}
