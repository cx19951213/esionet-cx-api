package com.esionet.vo;

import java.util.List;

/**
 *
 * @Description: 菜单返回体
 * @since chenxuan, 2019/9/26
 */
public class MenuVo {
    private String route;
    private String icon;
    private String name;
    private List<MenuVo> children;

    public String getRoute() {
        return route;
    }

    public void setRoute(String route) {
        this.route = route;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<MenuVo> getChildren() {
        return children;
    }

    public void setChildren(List<MenuVo> children) {
        this.children = children;
    }
}
