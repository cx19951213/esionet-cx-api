package com.esionet.common.config;


import com.esionet.common.api.interceptor.AdminInterceptor;
import com.esionet.common.api.interceptor.ApiInterceptor;
import com.esionet.common.api.resolver.AdminArgumentResolver;
import com.esionet.common.api.resolver.ApiArgumentResolver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;

import java.util.List;


/**
 * MVC配置
 *
 * @author chenxuan
 * @date 2017-02-10 22:30
 */
@Configuration
public class InterceptorConfig extends WebMvcConfigurationSupport {
    @Autowired
    private AdminInterceptor adminInterceptor;
    @Autowired
    private ApiInterceptor apiInterceptor;
    @Autowired
    private AdminArgumentResolver adminArgumentResolver;
    @Autowired
    private ApiArgumentResolver apiArgumentResolver;

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        //后台管理接口拦截器处理
        registry.addInterceptor(adminInterceptor).addPathPatterns("/admin/**");
        //前端接口拦截器处理
        registry.addInterceptor(apiInterceptor).addPathPatterns("/api/**");
    }

    @Override
    public void addArgumentResolvers(List<HandlerMethodArgumentResolver> argumentResolvers) {
        argumentResolvers.add(adminArgumentResolver);
        argumentResolvers.add(apiArgumentResolver);
    }

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/static/**").addResourceLocations("classpath:/static/");
        // 解决 SWAGGER 404报错
        registry.addResourceHandler("/swagger-ui.html").addResourceLocations("classpath:/META-INF/resources/");
        registry.addResourceHandler("/doc.html").addResourceLocations("classpath:/META-INF/resources/doc.html");
        registry.addResourceHandler("/webjars/**").addResourceLocations("classpath:/META-INF/resources/webjars/");
    }
}