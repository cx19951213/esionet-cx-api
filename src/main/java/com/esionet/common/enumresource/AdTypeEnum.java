package com.esionet.common.enumresource;


import com.esionet.common.utils.bean.EnumMessage;

/**
 * Created by 陈熠s
 * 2017/7/20.
 */
public enum AdTypeEnum implements EnumMessage {
    HOME_CAROUSEL("1","首页轮播"),
    OTHER("2","其他广告位");
    private final String value;
    private final String label;
    private AdTypeEnum(String value, String label) {
        this.label = label;
        this.value = value;
    }
    @Override
    public String getLabel() { return label;}
    @Override
    public String getValue() { return value; }
}
