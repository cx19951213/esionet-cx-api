package com.esionet.common.enumresource;

import com.esionet.common.utils.bean.EnumMessage;

/**
 * Created by 陈熠s
 * 2017/7/20.
 */
public enum WhetherEnum implements EnumMessage {
    YES("1","是"),
    NO("0","否");
    private final String label;
    private final String value;
    private WhetherEnum(String value, String label) {
        this.label = label;
        this.value = value;
    }
    @Override
    public String getLabel() { return label;}
    @Override
    public String getValue() { return value; }
}
