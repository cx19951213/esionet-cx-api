package com.esionet.common.enumresource;

import com.esionet.common.utils.bean.EnumMessage;

/**
 * Created by 陈熠s
 * 2017/7/20.
 */
public enum ColumnTypeEnum implements EnumMessage {
    STRING("1","字符串"),
    INTEGER("2","数字"),
    DATE("3","日期"),
    BOOLEAN("4","布尔"),
    ;
    private final String label;
    private final String value;
    private ColumnTypeEnum(String value, String label) {
        this.label = label;
        this.value = value;
    }
    @Override
    public String getLabel() { return label;}
    @Override
    public String getValue() { return value; }
}
