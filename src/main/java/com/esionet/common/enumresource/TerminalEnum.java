package com.esionet.common.enumresource;

import com.esionet.common.utils.bean.EnumMessage;

/**
 * Created by 陈熠s
 * 2017/7/20.
 */
public enum TerminalEnum implements EnumMessage {
    IOS("1","IOS"),
    ANDROID("2","安卓"),
    PC("3","PC端"),
    WEI_XIN("4","微信小程序"),
    ;
    private final String label;
    private final String value;
    private TerminalEnum(String value, String label) {
        this.label = label;
        this.value = value;
    }
    @Override
    public String getLabel() { return label;}
    @Override
    public String getValue() { return value; }
}
