package com.esionet.common.enumresource;

import com.esionet.common.utils.bean.EnumMessage;

/**
 * Created by 陈熠s
 * 2017/7/20.
 */
public enum DataTypeEnum implements EnumMessage {
    GRHD("1","个人话单"),
    JZHD("2","基账话单"),
    ZD("3","账单"),
    ;
    private final String label;
    private final String value;
    private DataTypeEnum(String value, String label) {
        this.label = label;
        this.value = value;
    }
    @Override
    public String getLabel() { return label;}
    @Override
    public String getValue() { return value; }
}
