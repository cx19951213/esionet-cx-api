package com.esionet.common.enumresource;

import com.esionet.common.utils.bean.EnumMessage;
/**
 * Created by 陈熠s
 * 2017/7/20.
 */
public enum StatusEnum implements EnumMessage {
    ENABLE("1","启用"),
    LIMIT("0","禁用");
    private final String label;
    private final String value;
    private StatusEnum(String value,String label) {
        this.label = label;
        this.value = value;
    }
    @Override
    public String getLabel() { return label;}
    @Override
    public String getValue() { return value; }
}
