package com.esionet.common.enumresource;


import com.esionet.common.utils.bean.EnumMessage;

/**
 * Created by 陈熠s
 * 2017/7/20.
 */
public enum MenuTypeEnum implements EnumMessage {
    CATALOG("1","目录"),
    MENU("2","菜单"),
    BUTTON("3","按钮");
    private final String value;
    private final String label;
    private MenuTypeEnum(String value, String label) {
        this.label = label;
        this.value = value;
    }
    @Override
    public String getLabel() { return label;}
    @Override
    public String getValue() { return value; }
}
