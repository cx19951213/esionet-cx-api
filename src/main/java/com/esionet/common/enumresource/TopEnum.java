package com.esionet.common.enumresource;


import com.esionet.common.utils.bean.EnumMessage;

/**
 * Created by 陈熠s
 * 2017/7/20.
 */
public enum TopEnum implements EnumMessage {
    ONE("1","一级分类"),
    TWO("2","二级分类"),
    THREE("3","三级分类");
    private final String value;
    private final String label;
    private TopEnum(String value, String label) {
        this.label = label;
        this.value = value;
    }
    @Override
    public String getLabel() { return label;}
    @Override
    public String getValue() { return value; }
}
