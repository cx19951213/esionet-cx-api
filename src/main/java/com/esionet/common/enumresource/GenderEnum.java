package com.esionet.common.enumresource;


import com.esionet.common.utils.bean.EnumMessage;

/**
 * Created by chenxuan
 * 2017/7/20.
 */
public enum GenderEnum implements EnumMessage {
    MEN("1","男"),
    WOMEN("2","女"),
    OTHER("0","未知");
    private final String label;
    private final String value;
    private GenderEnum( String value,String label) {
        this.label = label;
        this.value = value;
    }
    @Override
    public String getLabel() { return label;}
    @Override
    public String getValue() { return value; }
}
