package com.esionet.common.enumresource;


import com.esionet.common.utils.bean.EnumMessage;

/**
 * Created by 陈熠s
 * 2017/7/20.
 */
public enum ButtonTypeEnum implements EnumMessage {
    HOME_NAV("1","首页导航"),
    OTHER("2","其他导航");
    private final String value;
    private final String label;
    private ButtonTypeEnum(String value, String label) {
        this.label = label;
        this.value = value;
    }
    @Override
    public String getLabel() { return label;}
    @Override
    public String getValue() { return value; }
}
