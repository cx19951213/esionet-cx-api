package com.esionet.common.enumresource;

import com.esionet.common.utils.bean.EnumMessage;

/**
 * Created by 陈熠s
 * 2017/7/20.
 */
public enum FileTypeEnum implements EnumMessage {
    PIC("pic","图片"),
    VIDEO("video","视频"),
    MUSIC("music","音乐"),
    DOC("doc","文档"),
    ZIP("zip","压缩包"),
    OTHER("unknown","其他"),
    ;
    private final String label;
    private final String value;
    private FileTypeEnum(String value, String label) {
        this.label = label;
        this.value = value;
    }
    @Override
    public String getLabel() { return label;}
    @Override
    public String getValue() { return value; }
}
