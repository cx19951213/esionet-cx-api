package com.esionet.common.utils.bean;

import lombok.Data;

@Data
public class ReturnState {
    private String state;//上传状态SUCCESS 一定要大写
    private String url;//上传地址
    private String title;//图片名称demo.jpg
    private String original;//图片名称demo.jpg
}
