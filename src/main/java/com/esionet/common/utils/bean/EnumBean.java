package com.esionet.common.utils.bean;

/**
 * Created by 陈熠
 * 2017/7/20.
 */
public class EnumBean {
    private String value;
    private String label;

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }
}
