package com.esionet.common.utils.bean;

import java.util.List;

/**
 * Created by 陈熠
 * 2017/7/12.
 */
public class TreeVo {
    private String label;
    private String value;
    private List<TreeVo> children;

    public TreeVo() {
    }

    public TreeVo(String label, String value, List<TreeVo> children) {
        this.label = label;
        this.value = value;
        this.children = children;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public List<TreeVo> getChildren() {
        return children;
    }

    public void setChildren(List<TreeVo> children) {
        this.children = children;
    }
}
