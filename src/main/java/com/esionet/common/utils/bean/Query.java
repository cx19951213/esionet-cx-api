package com.esionet.common.utils.bean;


import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * 查询参数
 *
 * @author chenxuan
 * @email 228112142@qq.com
 * @date 2017-03-14 23:15
 */
public class Query extends LinkedHashMap<String, Object> {
	private static final long serialVersionUID = 1L;
	//当前页码
    private int page;
    //每页条数
    private int limit;

    public Query(Map<String, Object> params){
        this.putAll(params);
        //日期搜索区域封装，约定sql中开始时间startDate，结束时间endDate
        Object searchDate = params.get("searchDate");
        if(searchDate!=null && ((ArrayList) searchDate).get(0) != null){
            this.put("startDate", ((ArrayList) searchDate).get(0));
            this.put("endDate", ((ArrayList) searchDate).get(1));
        }
        //分页参数
        this.page = Integer.parseInt(params.get("page").toString());
        this.limit = Integer.parseInt(params.get("limit").toString());
        this.put("offset", (page - 1) * limit);
        this.put("page", page);
        this.put("limit", limit);

    }


    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getLimit() {
        return limit;
    }

    public void setLimit(int limit) {
        this.limit = limit;
    }
}
