package com.esionet.common.utils.bean;

/**
 * Created by 陈熠
 * 2017/7/19.
 */
public interface EnumMessage {
     String getLabel();
     String getValue();
}
