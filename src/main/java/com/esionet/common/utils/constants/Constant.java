package com.esionet.common.utils.constants;

/**
 * 常量
 * 
 * @author chenxuan
 * @email 228112142@qq.com
 * @date 2016年11月15日 下午1:23:52
 */
public class Constant {
    /**包路径**/
    public final static String PACKAGE_NAME = "com.esionet.common.enumresource";
	/** 超级管理员ID */
	public static final String SUPER_ADMIN = "1";
	/** 系统管理员默认密码 */
	public static final String INIT_PASSWORD = "123456";

	public static final String CURRENT_SYSTEM_USER = "CURRENT_SYSTEM_USER";
	public static final String CURRENT_ACCOUNT = "CURRENT_ACCOUNT";
}
