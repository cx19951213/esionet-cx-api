package com.esionet.common.utils.constants;

/**
 * http状态码常量
 * 
 * @author chenxuan
 * @email 228112142@qq.com
 * @date 2016年11月15日 下午1:23:52
 */
public class HttpConstant {
    /**成功**/
    public final static int SUCCESS = 200;
	/** 系统错误 */
	public static final int ERROR = 500;
	/** 权限不足 */
	public static final int AUTH_FAIL = 401;
	/** 支付失败 */
	public static final int PAYMENT_FAIL = 402;
}
