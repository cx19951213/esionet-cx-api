package com.esionet.common.utils.constants;

/**
 * 订单常量
 * 
 * @author chenxuan
 * @email 228112142@qq.com
 * @date 2016年11月15日 下午1:23:52
 */
public class OrderConstant {
	/**订单前缀*/
	public static final String WX_PAY = "wx";
	public static final String ALI_PAY = "a";

	/**微信余额充值回调路径*/
	public static final String WX_RECHARGE_NOTIFY_URL = "api/recharge/wxCallback";
	/**商品订单支付回调路径*/
	public static final String WX_PRODUCT_NOTIFY_URL = "api/order/wxCallback";
}
