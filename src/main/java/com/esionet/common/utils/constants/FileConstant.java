package com.esionet.common.utils.constants;

/**
 * 常量
 * 
 * @author chenxuan
 * @email 228112142@qq.com
 * @date 2016年11月15日 下午1:23:52
 */
public class FileConstant {
    /**文件名重复编码**/
    public final static int REPEAT_CODE = 100;
}
