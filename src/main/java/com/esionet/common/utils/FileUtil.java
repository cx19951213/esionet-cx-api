package com.esionet.common.utils;

import com.esionet.common.enumresource.FileTypeEnum;
import org.apache.commons.lang.StringUtils;

import java.util.HashMap;
import java.util.Map;

/**
 * @author chenxuan
 *         2017/11/18.
 */
public class FileUtil {

    public final static Map<String, String> FILE_TYPE_MAP = new HashMap<>();

    static {
        getAllFileType();  //初始化文件类型信息
    }

    /**
     * 初始化文件类型
     *
     * @author chenxuan
     */
    private static void getAllFileType() {
        FILE_TYPE_MAP.put("jpg", FileTypeEnum.PIC.getValue()); //JPEG (jpg)
        FILE_TYPE_MAP.put("png", FileTypeEnum.PIC.getValue());  //PNG (png)
        FILE_TYPE_MAP.put("gif", FileTypeEnum.PIC.getValue());  //GIF (gif)
        FILE_TYPE_MAP.put("tif", FileTypeEnum.PIC.getValue());  //TIFF (tif)
        FILE_TYPE_MAP.put("bmp", FileTypeEnum.PIC.getValue()); //Windows Bitmap (bmp)
        FILE_TYPE_MAP.put("zip", FileTypeEnum.ZIP.getValue());
        FILE_TYPE_MAP.put("rar", FileTypeEnum.ZIP.getValue());
        FILE_TYPE_MAP.put("txt", FileTypeEnum.DOC.getValue());
        FILE_TYPE_MAP.put("text", FileTypeEnum.DOC.getValue());
        FILE_TYPE_MAP.put("xlsx", FileTypeEnum.DOC.getValue());
        FILE_TYPE_MAP.put("xls", FileTypeEnum.DOC.getValue());  //MS Word
        FILE_TYPE_MAP.put("doc", FileTypeEnum.DOC.getValue());  //MS Excel 注意：word 和 excel的文件头一样
        FILE_TYPE_MAP.put("pdf", FileTypeEnum.DOC.getValue());  //Adobe Acrobat (pdf)
        FILE_TYPE_MAP.put("wav", FileTypeEnum.VIDEO.getValue());  //Wave (wav)
        FILE_TYPE_MAP.put("avi", FileTypeEnum.VIDEO.getValue());
        FILE_TYPE_MAP.put("rm", FileTypeEnum.VIDEO.getValue());  //Real Media (rm)
        FILE_TYPE_MAP.put("mpg", FileTypeEnum.VIDEO.getValue());  //
        FILE_TYPE_MAP.put("mov", FileTypeEnum.VIDEO.getValue());  //Quicktime (mov)
        FILE_TYPE_MAP.put("mp3", FileTypeEnum.MUSIC.getValue());
        FILE_TYPE_MAP.put("wma", FileTypeEnum.MUSIC.getValue());
    }

    /**
     * 获取文件类型
     * @param fileName 文件名
     * @author chenxuan
     */
    public static String getFileType(String fileName)  {
        String suffix= getFileSuffix(fileName);
        if(StringUtils.isBlank(suffix)){
            return FileTypeEnum.OTHER.getValue();
        }
        String fileType = FILE_TYPE_MAP.get(suffix);
        return fileType == null?FileTypeEnum.OTHER.getValue():fileType;
    }
    /**
     * 判断文件是否为图片
     *
     * @param fileName 文件名
     * @return 检查后的结果
     * @throws Exception
     * @author chenxuan
     */
    public static boolean isPicture(String fileName) throws Exception {
        // 文件名称为空的场合
        if (StringUtil.isEmpty(fileName)) {
            // 返回不和合法
            return false;
        }
        // 获得文件后缀名
        String tmpName = getFileSuffix(fileName);
        // 声明图片后缀名数组
        String imgeArray[] = {"bmp", "gif", "jpe", "jpeg", "jpg", "png", "tif", "tiff", "ico"};
        // 遍历名称数组
        for (int i = 0; i < imgeArray.length; i++) {
            // 判断单个类型文件的场合
            if (imgeArray[i].equals(tmpName.toLowerCase())) {
                return true;
            }

        }
        return false;
    }

    /**
     * 获取文件h后缀
     * @param fileName 文件名
     * @author chenxuan
     */
    public static String getFileSuffix(String fileName)  {
        // 获得文件后缀名
       return fileName.substring(fileName.lastIndexOf(".") + 1, fileName.length());
    }

}
