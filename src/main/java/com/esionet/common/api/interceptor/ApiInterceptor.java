package com.esionet.common.api.interceptor;

import com.esionet.common.api.annotation.IgnoreAuth;
import com.esionet.common.exception.MyException;
import com.esionet.common.utils.constants.Constant;
import com.esionet.common.utils.constants.HttpConstant;
import com.esionet.dao.mongo.AccountRepository;
import com.esionet.entity.dto.AccountDto;
import com.esionet.service.AccountService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 权限(Token)验证
 * @author chenxuan
 * @email 228112142@qq.com
 * @date 2017-03-23 15:38
 */
@Component
public class ApiInterceptor extends HandlerInterceptorAdapter {
    @Autowired
    private AccountRepository accountRepository;
    @Autowired
    private AccountService accountService;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        IgnoreAuth annotation;
        if(handler instanceof HandlerMethod) {
            annotation = ((HandlerMethod) handler).getMethodAnnotation(IgnoreAuth.class);
        }else{
            return true;
        }

        //如果有@IgnoreAuth注解，则不验证token
        if(annotation != null){
            return true;
        }
        //从header中获取token
        String token = request.getHeader("token");
        //如果header中不存在token，则从参数中获取token
        if(StringUtils.isBlank(token)){
            token = request.getParameter("token");
        }
        //token为空
        if(StringUtils.isBlank(token)){
            throw new MyException("token不能为空",HttpConstant.AUTH_FAIL);
        }

//        //查询token信息
        AccountDto account = accountRepository.findByToken(token);
        if (account == null){
            account = accountService.findByToken(token);
        }
        if(account == null){
            throw new MyException("token失效，请重新登录", HttpConstant.AUTH_FAIL);
        }

        //设置userId到request里，后续根据loginName，获取用户信息
        request.setAttribute(Constant.CURRENT_ACCOUNT, account);
        return true;
    }
}
