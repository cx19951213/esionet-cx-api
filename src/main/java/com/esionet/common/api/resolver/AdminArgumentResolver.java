package com.esionet.common.api.resolver;
import com.esionet.common.api.annotation.CurrentSysUser;
import com.esionet.common.utils.constants.Constant;
import com.esionet.entity.SysUser;
import org.springframework.core.MethodParameter;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.support.WebDataBinderFactory;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.method.support.ModelAndViewContainer;

/**
 * 有@GetSysUser注解的方法参数，注入当前系统登录用户
 * @author chenxuan
 * @date 2019-10-23 22:02
 */
@Component
public class AdminArgumentResolver implements HandlerMethodArgumentResolver {

    @Override
    public boolean supportsParameter(MethodParameter parameter) {
        return parameter.getParameterType().isAssignableFrom(SysUser.class) &&parameter.hasParameterAnnotation(CurrentSysUser.class);
    }

    @Override
    public Object resolveArgument(MethodParameter parameter, ModelAndViewContainer container,
                                  NativeWebRequest request, WebDataBinderFactory factory) throws Exception {
        //获取用户账号
        return request.getAttribute(Constant.CURRENT_SYSTEM_USER, RequestAttributes.SCOPE_REQUEST);
    }
}
