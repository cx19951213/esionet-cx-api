package com.esionet.common.api.annotation;

import java.lang.annotation.*;

/**
 * 请求需要的权限
 * @author chenxuan
 * @date 2019-03-23 15:44
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface RequireAuth {
    String value() default "";
}
