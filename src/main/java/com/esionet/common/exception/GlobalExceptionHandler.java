package com.esionet.common.exception;


import org.apache.shiro.authz.AuthorizationException;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import com.esionet.common.utils.bean.R;

import javax.servlet.http.HttpServletRequest;

/**
 * @author chenxuan
 * 统一异常处理
 * @date 2017/12/29
 */
@RestControllerAdvice
public class GlobalExceptionHandler {


    @ExceptionHandler(MyException.class)
    public R handleMyException(HttpServletRequest req,MyException e){
        R r = new R();
        r.put("code", e.getCode());
        r.put("msg", e.getMsg());
        r.put("detail",e.getMessage());
        r.put("url",req.getRequestURL().toString());

        return r;
    }

    @ExceptionHandler(DuplicateKeyException.class)
    public R handleDuplicateKeyException(DuplicateKeyException e){
        return R.error("数据库中已存在该记录");
    }

    @ExceptionHandler(AuthorizationException.class)
    public R handleAuthorizationException(AuthorizationException e){
        return R.error("没有权限，请联系管理员授权");
    }

    @ExceptionHandler(Exception.class)
    public R handleException(Exception e){
        return R.error();
    }

}
