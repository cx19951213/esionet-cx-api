package com.esionet.common.exception;

import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;


/**
 * @author: chenxuan
 * @Email: 228112142@qq.com
 * @Description:
 * @Date: 2018/1/6 14:46
 */
@Controller
@RequestMapping("/error")
public class BaseErrorPage  implements ErrorController {

    @Override
    public String getErrorPath() {
        return "/404.html";
    }

    @RequestMapping
    public String error() {
        return getErrorPath();
    }
}