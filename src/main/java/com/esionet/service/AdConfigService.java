package com.esionet.service;

import com.esionet.entity.AdConfig;

import java.util.List;
import java.util.Map;

/**
 * 小程序广告配置
 * @author chenxuan
 * @date 2019-11-05 16:28:50
 */
public interface AdConfigService {

	AdConfig get(String id);

    List<AdConfig> getList(Map<String, Object> map);

    int getCount(Map<String, Object> map);

    int getCount(AdConfig adConfig);

    void save(AdConfig adConfig);

    void update(AdConfig adConfig);

    void delete(String id);

    void deleteByParams(Map<String, Object> map);

    void deleteBatch(String[] ids);

    void updateStatus(String[] ids, String statusValue);

    List<AdConfig> getListByType(String type);
}
