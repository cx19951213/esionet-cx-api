package com.esionet.service;

import com.esionet.entity.SysLog;

import java.util.List;
import java.util.Map;

/**
 * 系统日志
 * 
 * @author chenxuan
 * @email 228112142@qq.com
 * @date 2017-03-08 10:40:56
 */
public interface SysLogService {
	
	SysLog get(Long id);
	
	List<SysLog> getList(Map<String, Object> map);
	
	int getCount(Map<String, Object> map);
	
	void save(SysLog sysLog);
	
	void update(SysLog sysLog);
	
	void delete(Long id);
	
	void deleteBatch(Long[] ids);

}
