package com.esionet.service;

import com.esionet.common.utils.bean.EnumBean;
import com.esionet.entity.SysDict;

import java.util.List;
import java.util.Map;

/**
 * 字典管理
 * 
 * @author chenxuan
 * @email qq228112142@qq.com
 * @date 2017-11-06 14:49:28
 */
public interface SysDictService {
	
	SysDict get(String paraId);

	List<SysDict> getList(Map<String, Object> map);
	
	int getCount(Map<String, Object> map);
	
	void save(SysDict sysDict);
	
	void update(SysDict sysDict);
	
	void delete(String paraId);
	
	void deleteBatch(String[] ods);

    void updateStatus(String[] ids, String statusValue);

	List<EnumBean>getCodeValues(Map<String, Object> params);

    List<SysDict> findByVerify(SysDict sysDict);

	List<Map<String,Object>> getDictNames();

}
