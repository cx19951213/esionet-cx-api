package com.esionet.service;

import com.esionet.common.utils.bean.R;
import com.esionet.entity.SysDept;

import java.util.List;
import java.util.Map;

/**
 * 部门
 * @author chenxuan
 * @date 2019-10-12 22:13:52
 */
public interface SysDeptService {

	SysDept get(String id);

    List<SysDept> getList(Map<String, Object> map);

    int getCount(Map<String, Object> map);

    int getCount(SysDept dept);

    void save(SysDept dept);

    void update(SysDept dept);

    void delete(String id);

    void deleteByParams(Map<String, Object> map);

    void deleteBatch(String[] ids);

    void updateStatus(String[] ids, String statusValue);

    R treeData();
}
