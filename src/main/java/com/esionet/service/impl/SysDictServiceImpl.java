package com.esionet.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.esionet.common.utils.bean.EnumBean;
import com.esionet.dao.SysDictDao;
import com.esionet.entity.SysDict;
import com.esionet.service.SysDictService;

import java.util.List;
import java.util.Map;


@Service("sysDictService")
@Transactional
public class SysDictServiceImpl implements SysDictService {
	@Autowired
	private SysDictDao sysDictDao;

	@Override
	public SysDict get(String paraId){
		return sysDictDao.get(paraId);
	}


	@Override
	public List<SysDict> getList(Map<String, Object> map){
		return sysDictDao.getList(map);
	}

	@Override
	public int getCount(Map<String, Object> map){
		return sysDictDao.getCount(map);
	}

	@Override
	public void save(SysDict sysDict){
		sysDictDao.save(sysDict);
	}

	@Override
	public void update(SysDict sysDict){
		sysDictDao.update(sysDict);
	}

	@Override
	public void delete(String paraId){
		sysDictDao.delete(paraId);
	}

	@Override
	public void deleteBatch(String[] ids){
		sysDictDao.deleteBatch(ids);
	}

	@Override
	public void updateStatus(String[] ids,String statusValue) {
		for (String id:ids){
			SysDict sysDict =new SysDict();
			sysDict.setId(id);
			sysDict.setStatus(statusValue);
			sysDictDao.update(sysDict);
		}
	}

	@Override
	public List<EnumBean> getCodeValues(Map<String, Object> params) {
		return sysDictDao.getCodeValues(params);
	}

	@Override
	public List<SysDict> findByVerify(SysDict sysDict) {
		return sysDictDao.findByVerify(sysDict);
	}

	@Override
	public  List<Map<String,Object>> getDictNames() {
		return sysDictDao.getDictNames();
	}

}
