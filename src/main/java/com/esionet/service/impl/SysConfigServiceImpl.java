package com.esionet.service.impl;

import com.alibaba.fastjson.JSON;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.esionet.common.enumresource.WhetherEnum;
import com.esionet.common.exception.MyException;
import com.esionet.dao.SysConfigDao;
import com.esionet.entity.SysConfig;
import com.esionet.service.SysConfigService;

import java.util.List;
import java.util.Map;

@Service("sysConfigService")
public class SysConfigServiceImpl implements SysConfigService {
	@Autowired
	private SysConfigDao sysConfigDao;
	
	@Override
	public void save(SysConfig config) {
		sysConfigDao.save(config);
	}

	@Override
	public void update(SysConfig config) {
		sysConfigDao.update(config);
	}


	@Override
	public void deleteBatch(String[] ids) {
		for (String id:ids) {
			SysConfig config = sysConfigDao.get(id);
			if(WhetherEnum.YES.getValue().equals(config.getIsSys())){
				throw new MyException("删除失败!存在系统内置参数");
			}
		}
		sysConfigDao.deleteBatch(ids);
	}

	@Override
	public List<SysConfig> getList(Map<String, Object> map) {
		return sysConfigDao.getList(map);
	}

	@Override
	public int getCount(Map<String, Object> map) {
		return sysConfigDao.getCount(map);
	}

	@Override
	public int getCount(SysConfig config) {
			return sysConfigDao.getCount(config);
	}

	@Override
	public SysConfig get(String id) {
		return sysConfigDao.get(id);
	}

	@Override
	public String getValue(String key) {
		return  sysConfigDao.getValue(key);
	}

	@Override
	public String getValue(String key, String defaultValue) {
		String value = sysConfigDao.getValue(key);
		if(StringUtils.isBlank(value)){
			return defaultValue;
		}
		return value;
	}

	@Override
	public SysConfig findByCode(String code) {
		return sysConfigDao.findByCode(code);
	}

	@Override
	public <T> T getConfigObject(String key, Class<T> clazz) {
		String value = getValue(key, null);
		if(StringUtils.isNotBlank(value)){
			return JSON.parseObject(value, clazz);
		}

		try {
			return clazz.newInstance();
		} catch (Exception e) {
			throw new MyException("获取参数失败");
		}
	}


}
