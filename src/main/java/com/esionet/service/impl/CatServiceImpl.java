package com.esionet.service.impl;

import com.esionet.common.enumresource.TopEnum;
import com.esionet.common.enumresource.WhetherEnum;
import com.esionet.common.exception.MyException;
import com.esionet.common.utils.bean.TreeVo;
import com.esionet.dao.CatDao;
import com.esionet.dao.mongo.CatRepository;
import com.esionet.entity.Cat;
import com.esionet.entity.dto.CatTreeDto;
import com.esionet.service.CatService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;


@Service("catService")
@Transactional
public class CatServiceImpl implements CatService {
	@Autowired
	private CatDao catDao;
	@Autowired
	private CatRepository catRepository;
	
	@Override
	public Cat get(String id){
		return catDao.get(id);
	}

	@Override
	public List<Cat> getList(Map<String, Object> map){
		return catDao.getList(map);
	}

	@Override
	public int getCount(Map<String, Object> map){
		return catDao.getCount(map);
	}

	@Override
	public int getCount(Cat cat){
		return catDao.getCount(cat);
	}

	@Override
	public void save(Cat cat){
		if ("0".equals(cat.getParentId())){
			cat.setCatLevel(TopEnum.ONE.getValue());
		}else {
			Cat parentCat = catDao.get(cat.getParentId());
			Integer parentCatLevel = Integer.parseInt(parentCat.getCatLevel());
			Integer catLevel = parentCatLevel + 1;
			cat.setCatLevel(catLevel.toString());
			if (catLevel==3 && StringUtils.isEmpty(cat.getCatImg())){
				throw new MyException("三级分类必须上传图片！");
			}
		}
		catDao.save(cat);

		catRepository.save(cat);
	}

	@Override
	public void update(Cat cat){
		if ("0".equals(cat.getParentId())){
			cat.setCatLevel(TopEnum.ONE.getValue());
		}else {
			Cat parentCat = catDao.get(cat.getParentId());
			Integer parentCatLevel = Integer.parseInt(parentCat.getCatLevel());
			Integer catLevel = parentCatLevel + 1;
			cat.setCatLevel(catLevel.toString());
			if (catLevel==3 && StringUtils.isEmpty(cat.getCatImg())){
				throw new MyException("三级分类必须上传图片！");
			}
		}
		catDao.update(cat);

		catRepository.save(cat);
	}

	@Override
	public void delete(String id){
		catDao.delete(id);
	}

    @Override
    public void deleteByParams(Map<String, Object> map){
			catDao.deleteByParams(map);
    }

    @Override
	public void deleteBatch(String[] ids){
		for (String id:ids){
			Cat cat=new Cat();
			cat.setId(id);
			cat.setIsDel(WhetherEnum.YES.getValue());
			catDao.update(cat);
			catRepository.deleteById(id);
        }
	}

    @Override
    public void updateStatus(String[] ids,String statusValue) {
//        for (String id:ids){
//			Cat cat=new Cat();
//			cat.setId(id);
//			cat.setStatus(statusValue);
//			catDao.update(cat);
//        }
    }

	@Override
	public List<TreeVo> getTreeData(boolean includeThree) {
		List<Cat> catList;
		//查询列表数据
		if(includeThree){
			catList = catDao.getList(null);
		}else{
			catList = catDao.queryNotThreeList();
		}

		List<TreeVo> treeData = new ArrayList<>();
		TreeVo top = new TreeVo();
		top.setLabel("顶级分类");
		top.setValue("0");
		for (Cat cat : catList) {
			if ("0".equals(cat.getParentId())) {
				TreeVo treeVo = new TreeVo(cat.getCatName(), cat.getId(), getChildren(catList, cat.getId()));
				treeData.add(treeVo);
			}
		}
		top.setChildren(treeData);
		List<TreeVo> data = new ArrayList<>();
		data.add(top);
		return data;
	}

	@Override
	public List<Cat> getRecommendCatList() {
		return catDao.getRecommendCatList();
	}

	@Override
	public List<CatTreeDto> getCatTree() {
		//查询列表数据



		Cat query = new Cat();
		query.setIsDel(WhetherEnum.NO.getValue());
		Example<Cat> catExample = Example.of(query);
		Sort sort = new Sort(Sort.Direction.DESC, "sortNo");
		List<Cat> catList = catRepository.findAll(catExample,sort);

		List<CatTreeDto> treeData = new ArrayList<>();
		List<CatTreeDto> recommendList = new ArrayList<>();
		CatTreeDto second = new CatTreeDto();

		CatTreeDto top = new CatTreeDto();
		top.setChildren(new ArrayList<CatTreeDto>(){{add(second); }});
		top.setCatName("推荐分类");
		treeData.add(top);

		for (Cat cat : catList) {
			if (WhetherEnum.YES.getValue().equals(cat.getIsRecommend())) {
				CatTreeDto catTreeDto = new CatTreeDto(cat.getId(), cat.getCatName(), cat.getCatImg(), getApiChildren(catList, cat.getId()));
				recommendList.add(catTreeDto);
			}
			if ("0".equals(cat.getParentId())) {
				CatTreeDto catTreeDto = new CatTreeDto(cat.getId(), cat.getCatName(), cat.getCatImg(), getApiChildren(catList, cat.getId()));
				treeData.add(catTreeDto);
			}
		}

		second.setChildren(recommendList);
		second.setCatName("推荐分类");

		return treeData;
	}

	//微信小程序下拉树
	private List<CatTreeDto> getApiChildren(List<Cat> catList, String parentId) {
		List<CatTreeDto> treeData = new ArrayList<>();
		for (Cat cat : catList) {
			if (parentId.equals(cat.getParentId())) {
				CatTreeDto catTreeDto = new CatTreeDto(cat.getId(), cat.getCatName(), cat.getCatImg(), getApiChildren(catList, cat.getId()));
				treeData.add(catTreeDto);
			}
		}

		return treeData;
	}

	//后台小程序下拉树
	private List<TreeVo> getChildren(List<Cat> catList, String parentId) {
		List<TreeVo> treeData = new ArrayList<>();
		for (Cat cat : catList) {
			if (parentId.equals(cat.getParentId())) {
				TreeVo treeVo = new TreeVo(cat.getCatName(), cat.getId(), getChildren(catList, cat.getId()));
				treeData.add(treeVo);
			}
		}

		return treeData;
	}

}
