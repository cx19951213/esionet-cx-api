package com.esionet.service.impl;

import com.esionet.common.enumresource.WhetherEnum;
import com.esionet.dao.ButtonConfigDao;
import com.esionet.dao.mongo.ButtonConfigRepository;
import com.esionet.entity.ButtonConfig;
import com.esionet.service.ButtonConfigService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;


@Service("buttonConfigService")
@Transactional
public class ButtonConfigServiceImpl implements ButtonConfigService {
	@Autowired
	private ButtonConfigDao buttonConfigDao;
	@Autowired
	private ButtonConfigRepository buttonConfigRepository;
	
	@Override
	public ButtonConfig get(String id){
		return buttonConfigDao.get(id);
	}

	@Override
	public List<ButtonConfig> getList(Map<String, Object> map){
		return buttonConfigDao.getList(map);
	}

	@Override
	public int getCount(Map<String, Object> map){
		return buttonConfigDao.getCount(map);
	}

	@Override
	public int getCount(ButtonConfig buttonConfig){
		return buttonConfigDao.getCount(buttonConfig);
	}

	@Override
	public void save(ButtonConfig buttonConfig){
		buttonConfigDao.save(buttonConfig);
		buttonConfigRepository.save(buttonConfig);
	}

	@Override
	public void update(ButtonConfig buttonConfig){
		buttonConfigDao.update(buttonConfig);
		buttonConfigRepository.save(buttonConfig);
	}

	@Override
	public void delete(String id){
		buttonConfigDao.delete(id);
	}

    @Override
    public void deleteByParams(Map<String, Object> map){
			buttonConfigDao.deleteByParams(map);
    }

    @Override
	public void deleteBatch(String[] ids){
		for (String id:ids){
			ButtonConfig buttonConfig=new ButtonConfig();
			buttonConfig.setId(id);
			buttonConfig.setIsDel(WhetherEnum.YES.getValue());
			buttonConfigDao.update(buttonConfig);
			buttonConfigRepository.deleteById(id);
		}
	}

    @Override
    public void updateStatus(String[] ids,String statusValue) {
        for (String id:ids){
			ButtonConfig buttonConfig=new ButtonConfig();
			buttonConfig.setId(id);
			buttonConfig.setStatus(statusValue);
			buttonConfigDao.update(buttonConfig);
        }
    }

	@Override
	public List<ButtonConfig> getListByType(String type) {
		return buttonConfigDao.getListByType(type);
	}

}
