package com.esionet.service.impl;

import com.esionet.common.enumresource.WhetherEnum;
import com.esionet.common.exception.MyException;
import com.esionet.common.utils.bean.R;
import com.esionet.common.utils.bean.TreeVo;
import com.esionet.entity.SysRole;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.esionet.dao.SysDeptDao;
import com.esionet.entity.SysDept;
import com.esionet.service.SysDeptService;




@Service("deptService")
@Transactional
public class SysDeptServiceImpl implements SysDeptService {
	@Autowired
	private SysDeptDao deptDao;
	
	@Override
	public SysDept get(String id){
		return deptDao.get(id);
	}

	@Override
	public List<SysDept> getList(Map<String, Object> map){
		return deptDao.getList(map);
	}

	@Override
	public int getCount(Map<String, Object> map){
		return deptDao.getCount(map);
	}

	@Override
	public int getCount(SysDept dept){
		return deptDao.getCount(dept);
	}

	@Override
	public void save(SysDept dept){
		deptDao.save(dept);
	}

	@Override
	public void update(SysDept dept){
		deptDao.update(dept);
	}

	@Override
	public void delete(String id){
		deptDao.delete(id);
	}

    @Override
    public void deleteByParams(Map<String, Object> map){
			deptDao.deleteByParams(map);
    }

    @Override
	public void deleteBatch(String[] ids){
		for (String deptId:ids) {
			SysDept dept = deptDao.get(deptId);
			if(WhetherEnum.YES.getValue().equals(dept.getIsSys())){
				throw new MyException("删除失败!存在系统内置部门");
			}
			Map<String,Object> params = new HashMap<>();
			params.put("parentId",deptId);
			List<SysDept> children = deptDao.getList(params);
			children.forEach(item ->{
				item.setParentId("0");
				deptDao.update(item);
			});
			deptDao.delete(deptId);
		}
	}

    @Override
    public void updateStatus(String[] ids,String statusValue) {
        for (String id:ids){
			SysDept dept=new SysDept();
			dept.setId(id);
			dept.setStatus(statusValue);
			deptDao.update(dept);
        }
    }

	@Override
	public R treeData() {
		List<SysDept> deptList = deptDao.getList(null);

		List<TreeVo> treeData = new ArrayList<>();
		TreeVo top = new TreeVo();
		top.setLabel("易选网络");
		top.setValue("0");
		for (SysDept dept : deptList) {
			if ("0".equals(dept.getParentId())) {
				TreeVo treeVo = new TreeVo(dept.getDeptName(), dept.getId(), getChildren(deptList, dept.getId()));
				treeData.add(treeVo);
			}
		}
		top.setChildren(treeData);
		List<TreeVo> data = new ArrayList<>();
		data.add(top);

		return R.ok().put("data", data);
	}


	private List<TreeVo> getChildren(List<SysDept> deptList, String parentId) {
		List<TreeVo> treeData = new ArrayList<>();
		for (SysDept dept : deptList) {
			if (parentId.equals(dept.getParentId())) {
				TreeVo treeVo = new TreeVo(dept.getDeptName(), dept.getId(), getChildren(deptList, dept.getId()));
				treeData.add(treeVo);
			}
		}

		return treeData;
	}

}
