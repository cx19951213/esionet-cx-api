package com.esionet.service.impl;

import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.esionet.common.utils.GenUtils;
import com.esionet.dao.SysGeneratorDao;
import com.esionet.service.SysGeneratorService;

import java.io.ByteArrayOutputStream;
import java.util.List;
import java.util.Map;
import java.util.zip.ZipOutputStream;

@Service("sysGeneratorService")
public class SysGeneratorServiceImpl implements SysGeneratorService {
	@Autowired
	private SysGeneratorDao sysGeneratorDao;

	@Override
	public List<Map<String, Object>> getList(Map<String, Object> map) {
		return sysGeneratorDao.getList(map);
	}

	@Override
	public int getCount(Map<String, Object> map) {
		return sysGeneratorDao.getCount(map);
	}

	@Override
	public Map<String, String> getTable(String tableName) {
		return sysGeneratorDao.getTable(tableName);
	}

	@Override
	public List<Map<String, String>> getColumns(String tableName) {
		return sysGeneratorDao.getColumns(tableName);
	}

	@Override
	public byte[] generatorCode(String[] tableNames) {
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		ZipOutputStream zip = new ZipOutputStream(outputStream);

		for(String tableName : tableNames){
			//查询表信息
			Map<String, String> table = this.getTable(tableName);
			//查询列信息
			List<Map<String, String>> columns = this.getColumns(tableName);
			//生成代码
			GenUtils.generatorCode(table, columns, zip);
		}
		IOUtils.closeQuietly(zip);
		return outputStream.toByteArray();
	}

	@Override
	public List<Map<String, String>> selectTables() {
		return sysGeneratorDao.selectTables();
	}

}
