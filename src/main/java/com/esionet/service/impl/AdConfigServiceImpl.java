package com.esionet.service.impl;

import com.esionet.common.enumresource.WhetherEnum;
import com.esionet.dao.AdConfigDao;
import com.esionet.dao.mongo.AdConfigRepository;
import com.esionet.entity.AdConfig;
import com.esionet.service.AdConfigService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;


@Service("adConfigService")
@Transactional
public class AdConfigServiceImpl implements AdConfigService {
	@Autowired
	private AdConfigDao adConfigDao;
	@Autowired
	private AdConfigRepository adConfigRepository;
	
	@Override
	public AdConfig get(String id){
		return adConfigDao.get(id);
	}

	@Override
	public List<AdConfig> getList(Map<String, Object> map){
		return adConfigDao.getList(map);
	}

	@Override
	public int getCount(Map<String, Object> map){
		return adConfigDao.getCount(map);
	}

	@Override
	public int getCount(AdConfig adConfig){
		return adConfigDao.getCount(adConfig);
	}

	@Override
	public void save(AdConfig adConfig){
		adConfigDao.save(adConfig);
		adConfigRepository.save(adConfig);
	}

	@Override
	public void update(AdConfig adConfig){
		adConfigDao.update(adConfig);
		adConfigRepository.save(adConfig);
	}

	@Override
	public void delete(String id){
		adConfigDao.delete(id);
	}

    @Override
    public void deleteByParams(Map<String, Object> map){
			adConfigDao.deleteByParams(map);
    }

    @Override
	public void deleteBatch(String[] ids){
		for (String id:ids){
			AdConfig adConfig=new AdConfig();
			adConfig.setId(id);
			adConfig.setIsDel(WhetherEnum.YES.getValue());
			adConfigDao.update(adConfig);
			adConfigRepository.deleteById(id);
		}
	}

    @Override
    public void updateStatus(String[] ids,String statusValue) {
        for (String id:ids){
			AdConfig adConfig=new AdConfig();
			adConfig.setId(id);
			adConfig.setStatus(statusValue);
			adConfigDao.update(adConfig);
        }
    }

	@Override
	public List<AdConfig> getListByType(String type) {
		return adConfigDao.getListByType(type);
	}

}
