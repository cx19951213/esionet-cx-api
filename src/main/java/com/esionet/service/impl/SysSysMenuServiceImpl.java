package com.esionet.service.impl;

import com.esionet.common.enumresource.MenuTypeEnum;
import com.esionet.common.utils.bean.TreeVo;
import com.esionet.entity.SysMenu;
import com.esionet.entity.SysUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.esionet.dao.SysMenuDao;
import com.esionet.service.SysMenuService;


@Service("menuService")
@Transactional
public class SysSysMenuServiceImpl implements SysMenuService {
    @Autowired
    private SysMenuDao sysMenuDao;


    @Override
    public SysMenu get(String menuId) {
        return sysMenuDao.get(menuId);
    }

    @Override
    public List<SysMenu> getList(Map<String, Object> map) {
        return sysMenuDao.getList(map);
    }

    @Override
    public int getCount(Map<String, Object> map) {
        return sysMenuDao.getCount(map);
    }

    @Override
    public int getCount(SysMenu sysMenu) {
        return sysMenuDao.getCount(sysMenu);
    }

    @Override
    public void save(SysMenu sysMenu) {
        sysMenuDao.save(sysMenu);
    }

    @Override
    public void update(SysMenu sysMenu) {
        sysMenuDao.update(sysMenu);
    }

    @Override
    public void delete(String menuId) {
        sysMenuDao.delete(menuId);
    }

    @Override
    public void deleteByParams(Map<String, Object> map) {
        sysMenuDao.deleteByParams(map);
    }

    @Override
    public void deleteBatch(String[] menuIds) {
        sysMenuDao.deleteBatch(menuIds);
    }

    @Override
    public void updateStatus(String[] ids, String statusValue) {
        for (String id : ids) {
            SysMenu sysMenu = new SysMenu();
            sysMenu.setMenuId(id);
            sysMenu.setStatus(statusValue);
            sysMenuDao.update(sysMenu);
        }
    }

    @Override
    public List<SysMenu> getMenuRoute() {
        List<SysMenu> routeList = new ArrayList<>();
        //获取菜单列表
        List<SysMenu> sysMenuList = sysMenuDao.queryNotButtonList();
        sysMenuList.forEach(sysMenu -> {
            //过滤出目录即路由设置
            if (MenuTypeEnum.CATALOG.getValue().equals(sysMenu.getType())) {
                //设置二级路由
                sysMenu.setChildren(getChildrenRouteList(sysMenuList,sysMenu.getMenuId()));
                routeList.add(sysMenu);
            }
        });

        return routeList;
    }

    private List<SysMenu> getChildrenRouteList(List<SysMenu> menuList,String parentId) {
        List<SysMenu> routeList = new ArrayList<>();
        //获取菜单列表
        menuList.forEach(sysMenu -> {
            //如果是目录
            if (parentId.equals(sysMenu.getParentId()) && MenuTypeEnum.MENU.getValue().equals(sysMenu.getType())) {
                routeList.add(sysMenu);
            }
        });
        return routeList;
    }

    @Override
    public List<SysMenu> getUserMenuList(String userId) {
        //系统管理员，拥有最高权限
        if ("1".equals(userId)) {
            return getAllMenuList(null);
        }

        //用户菜单列表
        List<String> menuIdList = sysMenuDao.queryAllMenuId(userId);
        return getAllMenuList(menuIdList);
    }

    @Override
    public List<TreeVo> getTreeData(boolean includeBtn) {
        List<SysMenu> sysMenuList;
        //查询列表数据
        if(includeBtn){
            sysMenuList = sysMenuDao.getList(null);
        }else{
            sysMenuList = sysMenuDao.queryNotButtonList();
        }


        List<TreeVo> treeData = new ArrayList<>();
        TreeVo top = new TreeVo();
        top.setLabel("顶级菜单");
        top.setValue("0");
        for (SysMenu sysMenu : sysMenuList) {
            if ("0".equals(sysMenu.getParentId())) {
                TreeVo treeVo = new TreeVo(sysMenu.getName(), sysMenu.getMenuId(), getChildren(sysMenuList, sysMenu.getMenuId()));
                treeData.add(treeVo);
            }
        }
        top.setChildren(treeData);
        List<TreeVo> data = new ArrayList<>();
        data.add(top);
        return data;
    }

    @Override
    public void insert(SysMenu sysMenu) {
        System.out.println("asd");
    }


    private List<TreeVo> getChildren(List<SysMenu> sysMenuList, String parentId) {
        List<TreeVo> treeData = new ArrayList<>();
        for (SysMenu sysMenu : sysMenuList) {
            if (parentId.equals(sysMenu.getParentId())) {
                TreeVo treeVo = new TreeVo(sysMenu.getName(), sysMenu.getMenuId(), getChildren(sysMenuList, sysMenu.getMenuId()));
                treeData.add(treeVo);
            }
        }

        return treeData;
    }

    /**
     * 获取所有菜单列表
     */
    private List<SysMenu> getAllMenuList(List<String> menuIdList) {
        //查询根菜单列表
        List<SysMenu> sysMenuList = queryListParentId("0", menuIdList);
        //递归获取子菜单
        getMenuTreeList(sysMenuList, menuIdList);

        return sysMenuList;
    }

    public List<SysMenu> queryListParentId(String parentId, List<String> menuIdList) {
        List<SysMenu> sysMenuList = sysMenuDao.queryListParentId(parentId);
        if (menuIdList == null) {
            return sysMenuList;
        }

        List<SysMenu> userSysMenuList = new ArrayList<>();
        for (SysMenu sysMenu : sysMenuList) {
            if (menuIdList.contains(sysMenu.getMenuId())) {
                userSysMenuList.add(sysMenu);
            }
        }
        return userSysMenuList;
    }

    /**
     * 递归
     */
    private List<SysMenu> getMenuTreeList(List<SysMenu> sysMenuList, List<String> menuIdList) {
        List<SysMenu> subSysMenuList = new ArrayList<SysMenu>();

        for (SysMenu entity : sysMenuList) {
            if (MenuTypeEnum.CATALOG.getValue().equals(entity.getType())) {//目录
                entity.setChildren(getMenuTreeList(queryListParentId(entity.getMenuId(), menuIdList), menuIdList));
            }
            subSysMenuList.add(entity);
        }

        return subSysMenuList;
    }
}
