package com.esionet.service.impl;

import com.esionet.common.enumresource.StatusEnum;
import com.esionet.common.enumresource.WhetherEnum;
import com.esionet.dao.mongo.AccountRepository;
import com.esionet.entity.dto.AccountDto;
import com.esionet.entity.dto.WxLoginDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.annotation.Id;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import com.esionet.dao.AccountDao;
import com.esionet.entity.Account;
import com.esionet.service.AccountService;




@Service("accountService")
@Transactional
public class AccountServiceImpl implements AccountService {
	@Autowired
	private AccountDao accountDao;
	@Autowired
	private AccountRepository accountRepository;
	
	@Override
	public Account get(String id){
		return accountDao.get(id);
	}

	@Override
	public List<Account> getList(Map<String, Object> map){
		return accountDao.getList(map);
	}

	@Override
	public int getCount(Map<String, Object> map){
		return accountDao.getCount(map);
	}

	@Override
	public int getCount(Account account){
		return accountDao.getCount(account);
	}

	@Override
	public void save(Account account){
		accountDao.save(account);
	}

	@Override
	public void update(Account account){
		accountDao.update(account);
		//保存到mongo
		accountRepository.save(account);
	}

	@Override
	public void delete(String id){
		accountDao.delete(id);
	}

    @Override
    public void deleteByParams(Map<String, Object> map){
			accountDao.deleteByParams(map);
    }

    @Override
	public void deleteBatch(String[] ids){
		accountDao.deleteBatch(ids);
	}

    @Override
    public void updateStatus(String[] ids,String statusValue) {
        for (String id:ids){
			Account account=new Account();
			account.setId(id);
			account.setStatus(statusValue);
			accountDao.update(account);
        }
    }

	@Override
	public AccountDto findByLoginName(String openId) {
		AccountDto  accountDto = accountRepository.findByLoginName(openId);
		if (accountDto == null){
			accountDto = accountDao.findByLoginName(openId);
		}
		return accountDto;
	}

	@Override
	public String wxRegister(WxLoginDto wxLoginDto, String openId, String ip) {
		Account account = new Account();
		account.setLoginName(openId);
		account.setNickName(wxLoginDto.getNickName());
		account.setAvatar(wxLoginDto.getAvatarUrl());
		account.setAddress(wxLoginDto.getAddress());
		account.setProvince(wxLoginDto.getProvince());
		account.setCity(wxLoginDto.getCity());
		account.setGender(wxLoginDto.getGender().toString());
		account.setIsShop(WhetherEnum.NO.getValue());
		account.setRegisterIp(ip);
		account.setLastLoginTime(new Date());
		account.setLastLoginIp(ip);
		account.setStatus(StatusEnum.ENABLE.getValue());
		account.setSortNo("0");
		account.setCreateTime(new Date());
		//生成一个token
		String token = UUID.randomUUID().toString();
		account.setToken(token);
		accountDao.save(account);

		//保存到mongo
		accountRepository.save(account);
		return token ;
	}

	@Override
	public AccountDto findByToken(String token) {
		return accountDao.findByToken(token);
	}

}
