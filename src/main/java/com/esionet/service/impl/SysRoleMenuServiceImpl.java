package com.esionet.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.esionet.dao.SysRoleMenuDao;
import com.esionet.service.SysRoleMenuService;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * 角色与菜单对应关系
 * 
 * @author chenxuan
 * @email 228112142@qq.com
 * @date 2016年9月18日 上午9:44:35
 */
@Service("sysRoleMenuService")
public class SysRoleMenuServiceImpl implements SysRoleMenuService {
	@Autowired
	private SysRoleMenuDao sysRoleMenuDao;

	@Override
	@Transactional
	public void saveOrUpdate(String roleId, List<String> menuIdList) {
		if(menuIdList==null||menuIdList.size() == 0){
			return ;
		}
		//先删除角色与菜单关系
		sysRoleMenuDao.delete(roleId);
		
		//保存角色与菜单关系
		Map<String, Object> map = new HashMap<>();
		map.put("roleId", roleId);
		map.put("menuIdList", menuIdList);
		sysRoleMenuDao.save(map);
	}

	@Override
	public List<String> getMenuIdList(String roleId) {
		return sysRoleMenuDao.getMenuIdList(roleId);
	}

}
