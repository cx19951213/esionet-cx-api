package com.esionet.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.esionet.common.enumresource.WhetherEnum;
import com.esionet.common.exception.MyException;
import com.esionet.common.utils.constants.Constant;
import com.esionet.dao.SysRoleDao;
import com.esionet.entity.SysRole;
import com.esionet.service.SysRoleMenuService;
import com.esionet.service.SysRoleService;
import com.esionet.service.SysUserService;

import java.util.Date;
import java.util.List;
import java.util.Map;


/**
 * 角色
 * 
 * @author chenxuan
 * @email 228112142@qq.com
 * @date 2016年9月18日 上午9:45:12
 */
@Service("sysRoleService")
public class SysRoleServiceImpl implements SysRoleService {
	@Autowired
	private SysRoleDao sysRoleDao;
	@Autowired
	private SysRoleMenuService sysRoleMenuService;
	@Autowired
	private SysUserService sysUserService;

	@Override
	public SysRole get(String roleId) {
		return sysRoleDao.get(roleId);
	}

	@Override
	public List<SysRole> getList(Map<String, Object> map) {
		return sysRoleDao.getList(map);
	}

	@Override
	public int getCount(Map<String, Object> map) {
		return sysRoleDao.getCount(map);
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public void save(SysRole role) {
		role.setCreateTime(new Date());
		sysRoleDao.save(role);

		//检查权限是否越权
		this.checkPrems(role);

		//保存角色与菜单关系
		sysRoleMenuService.saveOrUpdate(role.getId(), role.getMenuIdList());
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public void update(SysRole role) {
		sysRoleDao.update(role);

		//检查权限是否越权
		this.checkPrems(role);

		//更新角色与菜单关系
		sysRoleMenuService.saveOrUpdate(role.getId(), role.getMenuIdList());
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public void deleteBatch(String[] roleIds) {
		for (String roleId:roleIds) {
			SysRole role = sysRoleDao.get(roleId);
			if(WhetherEnum.YES.getValue().equals(role.getIsSys())){
				throw new MyException("删除失败!存在系统内置角色");
			}
		}
		sysRoleDao.deleteBatch(roleIds);
	}

	@Override
	public List<String> queryRoleIdList(String createUserId) {
		return sysRoleDao.queryRoleIdList(createUserId);
	}

	@Override
	public List<SysRole> findAll(Map<String, Object> params) {
		return sysRoleDao.findAll(params);
	}

	/**
	 * 检查权限是否越权
	 */
	private void checkPrems(SysRole role){
		//如果不是超级管理员，则需要判断角色的权限是否超过自己的权限
		if(role.getCreateUserId().equals(Constant.SUPER_ADMIN)){
			return ;
		}

		//查询用户所拥有的菜单列表
		List<String> menuIdList = sysUserService.queryAllMenuId(role.getCreateUserId(),null);
		
		//判断是否越权
		if(!menuIdList.containsAll(role.getMenuIdList())){
			throw new MyException("新增角色的权限，已超出你的权限范围");
		}
	}
}
