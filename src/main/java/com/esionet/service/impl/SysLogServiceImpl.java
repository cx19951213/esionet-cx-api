package com.esionet.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.esionet.dao.SysLogDao;
import com.esionet.entity.SysLog;
import com.esionet.service.SysLogService;

import java.util.List;
import java.util.Map;


@Service("sysLogService")
public class SysLogServiceImpl implements SysLogService {
	@Autowired
	private SysLogDao sysLogDao;
	
	@Override
	public SysLog get(Long id){
		return sysLogDao.get(id);
	}
	
	@Override
	public List<SysLog> getList(Map<String, Object> map){
		return sysLogDao.getList(map);
	}
	
	@Override
	public int getCount(Map<String, Object> map){
		return sysLogDao.getCount(map);
	}
	
	@Override
	public void save(SysLog sysLog){
		sysLogDao.save(sysLog);
	}
	
	@Override
	public void update(SysLog sysLog){
		sysLogDao.update(sysLog);
	}
	
	@Override
	public void delete(Long id){
		sysLogDao.delete(id);
	}
	
	@Override
	public void deleteBatch(Long[] ids){
		sysLogDao.deleteBatch(ids);
	}
	
}
