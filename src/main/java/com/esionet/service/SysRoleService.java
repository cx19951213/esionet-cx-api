package com.esionet.service;

import com.esionet.entity.SysRole;

import java.util.List;
import java.util.Map;


/**
 * 角色
 * 
 * @author chenxuan
 * @email 228112142@qq.com
 * @date 2016年9月18日 上午9:42:52
 */
public interface SysRoleService {
	
	SysRole get(String roleId);
	
	List<SysRole> getList(Map<String, Object> map);
	
	int getCount(Map<String, Object> map);
	
	void save(SysRole role);
	
	void update(SysRole role);
	
	void deleteBatch(String[] roleIds);
	
	/**
	 * 查询用户创建的角色ID列表
	 */
	List<String> queryRoleIdList(String createUserId);

    List<SysRole> findAll(Map<String, Object> params);
}
