package com.esionet.service;

import com.esionet.entity.ButtonConfig;

import java.util.List;
import java.util.Map;

/**
 * 小程序按钮配置
 * @author chenxuan
 * @date 2019-11-05 14:10:24
 */
public interface ButtonConfigService {

	ButtonConfig get(String id);

    List<ButtonConfig> getList(Map<String, Object> map);

    int getCount(Map<String, Object> map);

    int getCount(ButtonConfig buttonConfig);

    void save(ButtonConfig buttonConfig);

    void update(ButtonConfig buttonConfig);

    void delete(String id);

    void deleteByParams(Map<String, Object> map);

    void deleteBatch(String[] ids);

    void updateStatus(String[] ids, String statusValue);

    List<ButtonConfig> getListByType(String type);
}
