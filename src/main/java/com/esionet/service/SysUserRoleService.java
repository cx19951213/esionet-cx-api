package com.esionet.service;

import java.util.List;


/**
 * 用户与角色对应关系
 * 
 * @author chenxuan
 * @email 228112142@qq.com
 * @date 2016年9月18日 上午9:43:24
 */
public interface SysUserRoleService {
	
	void saveOrUpdate(String userId, String[] roleIdList);
	
	/**
	 * 根据用户ID，获取角色ID列表
	 */
	List<String> queryRoleIdList(String userId);
	
	void delete(String userId);
}
