package com.esionet.service;

import com.esionet.common.utils.bean.TreeVo;
import com.esionet.entity.Cat;
import com.esionet.entity.dto.CatTreeDto;

import java.util.List;
import java.util.Map;

/**
 * 商品分类
 * @author chenxuan
 * @date 2019-10-29 17:09:14
 */
public interface CatService {

	Cat get(String id);

    List<Cat> getList(Map<String, Object> map);

    int getCount(Map<String, Object> map);

    int getCount(Cat cat);

    void save(Cat cat);

    void update(Cat cat);

    void delete(String id);

    void deleteByParams(Map<String, Object> map);

    void deleteBatch(String[] ids);

    void updateStatus(String[] ids, String statusValue);

    List<TreeVo> getTreeData(boolean includeBtn);

    List<Cat> getRecommendCatList();

    List<CatTreeDto> getCatTree();
}
