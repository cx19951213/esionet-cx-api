package com.esionet.service;

import java.util.List;
import java.util.Map;

/**
 * 代码生成器
 * 
 * @author chenxuan
 * @email 228112142@qq.com
 * @date 2016年12月19日 下午3:33:38
 */
public interface SysGeneratorService {
	
	List<Map<String, Object>> getList(Map<String, Object> map);
	
	int getCount(Map<String, Object> map);
	
	Map<String, String> getTable(String tableName);
	
	List<Map<String, String>> getColumns(String tableName);
	
	/**
	 * 生成代码
	 */
	byte[] generatorCode(String[] tableNames);

	List<Map<String,String>> selectTables();
}
