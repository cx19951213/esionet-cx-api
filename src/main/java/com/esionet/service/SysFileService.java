package com.esionet.service;

import com.esionet.common.utils.bean.Query;
import com.esionet.common.utils.bean.R;
import com.esionet.common.utils.bean.TreeVo;
import com.esionet.entity.SysFile;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Map;

/**
 * 文件
 * @author chenxuan
 * @date 2019-07-03 21:08:27
 */
public interface SysFileService {

	SysFile get(String id);

    List<SysFile> getList(Map<String, Object> map);

    int getCount(Map<String, Object> map);

    int getCount(SysFile file);

    void save(SysFile file);

    void update(SysFile file);

    void delete(String id);

    void deleteByParams(Map<String, Object> map);

    void deleteBatch(String[] ids);

    void updateStatus(String[] ids, String statusValue);

    R addFolder(String parentId, String fileName);

    void addToRecycle(String[] ids);

    void rename(String fileId, String newSysFileName);

    List<TreeVo> selectFolders(String fileId);

    R moveFile(String fileId, String parentId);

    List<SysFile> getRecycleList(Query query);

    void restoreFile(String[] ids);

    void clearRecycle();

    R upload(MultipartFile file, String parentId);

    List<SysFile> getImgList(Query query);
}
