package com.esionet.service;

import com.esionet.common.utils.bean.TreeVo;
import com.esionet.entity.SysMenu;

import java.util.List;
import java.util.Map;

/**
 * 菜单
 * @author chenxuan
 * @date 2019-09-26 14:37:29
 */
public interface SysMenuService {

	SysMenu get(String menuId);

    List<SysMenu> getList(Map<String, Object> map);

    int getCount(Map<String, Object> map);

    int getCount(SysMenu sysMenu);

    void save(SysMenu sysMenu);

    void update(SysMenu sysMenu);

    void delete(String menuId);

    void deleteByParams(Map<String, Object> map);

    void deleteBatch(String[] menuIds);

    void updateStatus(String[] ids, String statusValue);

    List<SysMenu> getUserMenuList(String userId);

    List<TreeVo> getTreeData(boolean includeBtn);

    void insert(SysMenu sysMenu);

    List<SysMenu> getMenuRoute();
}
