package com.esionet.service;

import com.esionet.common.utils.bean.R;
import com.esionet.entity.SysUser;

import java.util.List;
import java.util.Map;


/**
 * 系统用户
 * 
 * @author chenxuan
 * @email 228112142@qq.com
 * @date 2016年9月18日 上午9:43:39
 */
public interface SysUserService {
	
	/**
	 * 查询用户的所有权限
	 * @param userId  用户ID
	 */
	List<String> queryAllPerms(String userId);
	
	/**
	 * 查询用户的所有菜单ID
	 */
	List<String> queryAllMenuId(String userId, String module);
	
	/**
	 * 根据用户名，查询系统用户
	 */
	SysUser queryByUserName(String username);
	
	/**
	 * 根据用户ID，查询用户
	 * @param userId
	 * @return
	 */
	SysUser get(String userId);
	/**
	 * 查询用户列表
	 */
	List<SysUser> getList(Map<String, Object> map);
	
	/**
	 * 查询总数
	 */
	int getCount(Map<String, Object> map);
	
	/**
	 * 保存用户
	 */
	void save(SysUser user);
	
	/**
	 * 修改用户
	 */
	void update(SysUser user);
	
	/**
	 * 删除用户
	 */
	void deleteBatch(String[] userIds);
	
	/**
	 * 修改密码
	 * @param userId       用户ID
	 * @param password     原密码
	 * @param newPassword  新密码
	 */
	int updatePassword(String userId, String password, String newPassword);

    void initPassword(String[] userIds);

	SysUser getInfo(String userId);

	void updateStatus(String[] ids, String stateValue);

	void updateMyInfo(SysUser user);

	SysUser findByToken(String token);

	R login(String username, String password);
}
