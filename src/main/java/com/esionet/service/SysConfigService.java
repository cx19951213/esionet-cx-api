package com.esionet.service;

import com.esionet.entity.SysConfig;

import java.util.List;
import java.util.Map;

/**
 * 系统配置信息
 * 
 * @author chenxuan
 * @email 228112142@qq.com
 * @date 2016年12月4日 下午6:49:01
 */
public interface SysConfigService {
	
	/**
	 * 保存配置信息
	 */
	public void save(SysConfig config);
	
	/**
	 * 更新配置信息
	 */
	public void update(SysConfig config);
	
	/**
	 * 删除配置信息
	 */
	public void deleteBatch(String[] ids);
	
	/**
	 * 获取List列表
	 */
	public List<SysConfig> getList(Map<String, Object> map);
	/**
	 * 获取总记录数
	 */
	public int getCount(Map<String, Object> map);

	public int getCount(SysConfig config);

	public SysConfig get(String id);

	
	/**
	 * 根据key，获取value的Object对象
	 * @param key    key
	 * @param clazz  Object对象
	 */
	public <T> T getConfigObject(String key, Class<T> clazz);

	/**
	 * 根据code，获取value
	 */
	public String getValue(String code);
	/**
	 * 根据code，获取value，如果没值返回defaultValue
	 */
	public String getValue(String code, String defaultValue);


	public SysConfig findByCode(String code);
}
