package com.esionet.service;

import com.esionet.entity.Account;
import com.esionet.entity.dto.AccountDto;
import com.esionet.entity.dto.WxLoginDto;

import java.util.List;
import java.util.Map;

/**
 * 用户信息
 * @author chenxuan
 * @date 2020-03-09 15:44:49
 */
public interface AccountService {

	Account get(String id);

    List<Account> getList(Map<String, Object> map);

    int getCount(Map<String, Object> map);

    int getCount(Account account);

    void save(Account account);

    void update(Account account);

    void delete(String id);

    void deleteByParams(Map<String, Object> map);

    void deleteBatch(String[] ids);

    void updateStatus(String[] ids, String statusValue);

    AccountDto findByLoginName(String openId);

    String wxRegister(WxLoginDto wxLoginDto, String openId ,String ip);

    AccountDto findByToken(String token);
}
