package com.esionet.dao;

import com.esionet.common.utils.bean.Query;
import com.esionet.entity.SysFile;

import java.util.List;

/**
 * 文件
 * @author chenxuan
 * @date 2019-07-03 21:08:27
 */
public interface SysFileDao extends BaseDao<SysFile> {

    List<SysFile> getRecycleList(Query query);

    void clearRecycle();

    List<SysFile> getImgList(Query query);
}
