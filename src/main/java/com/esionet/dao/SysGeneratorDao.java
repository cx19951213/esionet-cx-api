package com.esionet.dao;

import java.util.List;
import java.util.Map;

/**
 * 代码生成器
 * 
 * @author chenxuan
 * @email 228112142@qq.com
 * @date 2016年12月19日 下午3:32:04
 */
public interface SysGeneratorDao {
	
	List<Map<String, Object>> getList (Map<String, Object> map);
	
	int getCount(Map<String, Object> map);
	
	Map<String, String> getTable(String tableName);
	
	List<Map<String, String>> getColumns(String tableName);

	List<Map<String,String>> selectTables();
}
