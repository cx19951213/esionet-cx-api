package com.esionet.dao;

import com.esionet.entity.SysMenu;

import java.util.List;

/**
 * 菜单
 * @author chenxuan
 * @date 2019-09-26 14:37:29
 */
public interface SysMenuDao extends BaseDao<SysMenu> {

    List<SysMenu> queryListParentId(String parentId);

    List<String> queryAllMenuId(String userId);

    List<SysMenu> queryNotButtonList();
}
