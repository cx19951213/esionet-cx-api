package com.esionet.dao;

import org.apache.ibatis.annotations.Param;
import com.esionet.entity.SysUser;

import java.util.List;
import java.util.Map;

/**
 * 系统用户
 * 
 * @author chenxuan
 * @email 228112142@qq.com
 * @date 2016年9月18日 上午9:34:11
 */
public interface SysUserDao extends BaseDao<SysUser> {
	
	/**
	 * 查询用户的所有权限
	 * @param userId  用户ID
	 */
	List<String> queryAllPerms(String userId);
	
	/**
	 * 查询用户的所有菜单ID
	 */
	List<String> queryAllMenuId(@Param("userId") String userId, @Param("module") String module);
	
	/**
	 * 根据用户名，查询系统用户
	 */
	SysUser queryByUserName(String username);
	
	/**
	 * 修改密码
	 */
	int updatePassword(Map<String, Object> map);

    SysUser getInfo(String userId);

	SysUser findByToken(String token);
}
