package com.esionet.dao;

import com.esionet.entity.Account;
import com.esionet.entity.dto.AccountDto;

/**
 * 用户信息
 * @author chenxuan
 * @date 2020-03-09 15:44:49
 */
public interface AccountDao extends BaseDao<Account> {

    AccountDto findByLoginName(String openId);

    AccountDto findByToken(String token);
}
