package com.esionet.dao;

import com.esionet.entity.AdConfig;

import java.util.List;

/**
 * 小程序广告配置
 * @author chenxuan
 * @date 2019-11-05 16:28:50
 */
public interface AdConfigDao extends BaseDao<AdConfig> {

    List<AdConfig> getListByType(String type);
}
