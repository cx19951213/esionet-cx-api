package com.esionet.dao;

import com.esionet.entity.Cat;

import java.util.List;

/**
 * 商品分类
 * @author chenxuan
 * @date 2019-10-29 17:09:14
 */
public interface CatDao extends BaseDao<Cat> {

    List<Cat> queryNotThreeList();

    List<Cat> getRecommendCatList();

    List<Cat> getCatTreeList();
}
