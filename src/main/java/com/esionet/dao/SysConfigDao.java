package com.esionet.dao;
import com.esionet.entity.SysConfig;

/**
 * 系统参数
 * 
 * @author chenxuan
 */
public interface SysConfigDao extends BaseDao<SysConfig> {

    SysConfig findByCode(String code);

    String getValue(String key);

}
