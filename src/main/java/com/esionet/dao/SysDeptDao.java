package com.esionet.dao;

import com.esionet.entity.SysDept;

/**
 * 部门
 * @author chenxuan
 * @date 2019-10-12 22:13:52
 */
public interface SysDeptDao extends BaseDao<SysDept> {
	
}
