package com.esionet.dao;

import com.esionet.common.utils.bean.EnumBean;
import com.esionet.entity.SysDict;

import java.util.List;
import java.util.Map;

/**
 * 字典管理
 * 
 * @author chenxuan
 * @email qq228112142@qq.com
 * @date 2017-11-06 14:49:28
 */
public interface SysDictDao extends BaseDao<SysDict> {

    List<EnumBean> getCodeValues(Map<String, Object> params);

    List<SysDict> findByVerify(SysDict sysDict);

    List<Map<String,Object>>  getDictNames();
}
