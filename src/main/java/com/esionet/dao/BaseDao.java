package com.esionet.dao;

import java.util.List;
import java.util.Map;

/**
 * 基础Dao(还需在XML文件里，有对应的SQL语句)
 * 
 * @author chenxuan
 * @email 228112142@qq.com
 * @date 2016年9月18日 上午9:31:36
 */
public interface BaseDao<T> {
	
	void save(T t);
	
	void save(Map<String, Object> map);
	
	void saveBatch(List<T> list);
	
	int update(T t);
	
	int update(Map<String, Object> map);
	
	int delete(Object id);
	
	int deleteByParams(Map<String, Object> map);
	
	int deleteBatch(Object[] id);

	T get(Object id);

	List<T> getList(Map<String, Object> map);

	int getCount(Map<String, Object> map);

	int getCount(T t);

}
