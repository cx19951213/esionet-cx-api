package com.esionet.dao;


import com.esionet.entity.SysLog;

/**
 * 系统日志
 * 
 * @author chenxuan
 * @email 228112142@qq.com
 * @date 2017-03-08 10:40:56
 */
public interface SysLogDao extends BaseDao<SysLog> {
	
}
