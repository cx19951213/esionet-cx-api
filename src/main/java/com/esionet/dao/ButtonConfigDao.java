package com.esionet.dao;

import com.esionet.entity.ButtonConfig;

import java.util.List;

/**
 * 小程序按钮配置
 * @author chenxuan
 * @date 2019-11-05 14:10:24
 */
public interface ButtonConfigDao extends BaseDao<ButtonConfig> {

    List<ButtonConfig> getListByType(String type);
}
