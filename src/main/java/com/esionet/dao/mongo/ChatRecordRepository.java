package com.esionet.dao.mongo;

import com.esionet.entity.dto.ChatRecordDto;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface ChatRecordRepository extends MongoRepository<ChatRecordDto,String> {
}
