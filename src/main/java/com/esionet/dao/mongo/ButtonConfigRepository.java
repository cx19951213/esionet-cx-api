package com.esionet.dao.mongo;

import com.esionet.entity.ButtonConfig;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ButtonConfigRepository extends MongoRepository<ButtonConfig,String>{
}
