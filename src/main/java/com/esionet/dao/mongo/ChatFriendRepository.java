package com.esionet.dao.mongo;

import com.esionet.entity.dto.ChatFriendDto;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface ChatFriendRepository extends MongoRepository<ChatFriendDto,String>{
    List<ChatFriendDto> findByUserId(String userId);
}
