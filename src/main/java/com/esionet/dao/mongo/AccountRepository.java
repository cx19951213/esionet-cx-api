package com.esionet.dao.mongo;

import com.esionet.entity.Account;
import com.esionet.entity.dto.AccountDto;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AccountRepository extends MongoRepository<Account,String>{
    AccountDto findByLoginName(String openId);

    AccountDto findByToken(String token);
}
