package com.esionet.dao.mongo;

import com.esionet.entity.Cat;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CatRepository  extends MongoRepository<Cat,String>{
}
