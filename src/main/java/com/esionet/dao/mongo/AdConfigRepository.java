package com.esionet.dao.mongo;

import com.esionet.entity.AdConfig;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AdConfigRepository extends MongoRepository<AdConfig,String>{
}
