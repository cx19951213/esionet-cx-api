package com.esionet.activemq;

import com.esionet.common.enumresource.WhetherEnum;
import com.esionet.dao.mongo.ChatFriendRepository;
import com.esionet.dao.mongo.ChatRecordRepository;
import com.esionet.entity.dto.ChatFriendDto;
import com.esionet.entity.dto.ChatRecordDto;
import net.sf.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

/**
 * 消费者
 */
@Component
public class ActiveMqCustomer {

    @Autowired
    private ChatRecordRepository chatRecordRepository;
    @Autowired
    private ChatFriendRepository chatFriendRepository;

    /**
     * 监听和接收  queue队列消息
     */
    @JmsListener(destination = "wxMsg")
    public void receiveWxMsg(String msg) {
        JSONObject json = JSONObject.fromObject(msg);
        ChatRecordDto chatRecordDto = (ChatRecordDto) JSONObject.toBean(json,ChatRecordDto.class);
        //新增聊天记录
        chatRecordDto.setId(chatRecordDto.getSender().getId()+chatRecordDto.getReceiver().getId()+chatRecordDto.getCreateTime());
        chatRecordDto.setSenderId(chatRecordDto.getSender().getId());
        chatRecordDto.setReceiverId(chatRecordDto.getReceiver().getId());
        chatRecordDto.setIsRead(WhetherEnum.NO.getValue());
        chatRecordRepository.save(chatRecordDto);

        //新增聊天好友记录
        //自己是接收者，好友是发送者
        ChatFriendDto userDto = new ChatFriendDto();
        userDto.setId(chatRecordDto.getSenderId()+"-"+chatRecordDto.getReceiverId());
        userDto.setUserId(chatRecordDto.getReceiverId());
        userDto.setFriendId(chatRecordDto.getSenderId());
        userDto.setHeadPic(chatRecordDto.getSender().getHeadPic());
        userDto.setNickName(chatRecordDto.getSender().getNickName());
        userDto.setLastContent(chatRecordDto.getContent());
        userDto.setLastTime(chatRecordDto.getCreateTime());

        ChatRecordDto userQuery = new ChatRecordDto();
        userQuery.setSenderId(chatRecordDto.getSenderId());
        userQuery.setReceiverId(chatRecordDto.getReceiverId());
        userQuery.setIsRead(WhetherEnum.NO.getValue());
        Example<ChatRecordDto> userExample = Example.of(userQuery);
        userDto.setCount(chatRecordRepository.count(userExample));
        chatFriendRepository.save(userDto);


        //自己是发送者，好友是接收者
        ChatFriendDto friendDto = new ChatFriendDto();
        friendDto.setId(chatRecordDto.getReceiverId()+"-"+chatRecordDto.getSenderId());
        friendDto.setUserId(chatRecordDto.getSenderId());
        friendDto.setFriendId(chatRecordDto.getReceiverId());
        friendDto.setHeadPic(chatRecordDto.getReceiver().getHeadPic());
        friendDto.setNickName(chatRecordDto.getReceiver().getNickName());
        friendDto.setLastContent(chatRecordDto.getContent());
        friendDto.setLastTime(chatRecordDto.getCreateTime());

        ChatRecordDto friendQuery = new ChatRecordDto();
        friendQuery.setSenderId(chatRecordDto.getReceiverId());
        friendQuery.setReceiverId(chatRecordDto.getSenderId());
        friendQuery.setIsRead(WhetherEnum.NO.getValue());
        Example<ChatRecordDto> friendExample = Example.of(userQuery);
        userDto.setCount(chatRecordRepository.count(friendExample));
        chatFriendRepository.save(friendDto);

        System.out.println("接受到：" + msg);
    }
}
