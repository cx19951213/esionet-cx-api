package com.esionet.activemq;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Service;
import javax.jms.Destination;

/**
 * 生产者
 */
@Service
public class ActiveMqProducer {
    @Autowired
    private JmsTemplate jmsTemplate;

    public void sendMessage(Destination destination, String message) {
        // 指定消息发送的目的地及内容
        jmsTemplate.convertAndSend(destination, message);
        System.out.println("消息发送成功！message=" + message);
    }

}
