package com.esionet.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import com.esionet.common.annotation.Version;

/**
 * @File: IndexController
 * @Author: chenxuan
 * @Version: 1.0
 * @Date: 2019/5/18
 * @Modify:
 * @Description:
 */
@Controller
public class IndexController {


    @Version
    @RequestMapping(value = "/index")
    public String index() {
        return "redirect:/doc.html";
    }

    @RequestMapping(value = "/")
    public String home() {
        return "redirect:/doc.html";
    }

}
