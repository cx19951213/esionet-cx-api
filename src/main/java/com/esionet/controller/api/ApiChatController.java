package com.esionet.controller.api;

import com.esionet.common.api.annotation.CurrentAccount;
import com.esionet.common.utils.bean.R;
import com.esionet.dao.mongo.ChatFriendRepository;
import com.esionet.dao.mongo.ChatRecordRepository;
import com.esionet.entity.dto.AccountDto;
import com.esionet.entity.dto.ChatFriendDto;
import com.esionet.entity.dto.ChatRecordDto;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @Description: 微信小程序-小程序聊天模块
 * @since chenxuan 2019-11-05 16:28:50
 */
@RestController
@RequestMapping("/api/chat")
@Api(tags = "微信小程序-小程序聊天模块")
public class ApiChatController {
	@Autowired
	private ChatFriendRepository chatFriendRepository;

	/**
	 * 获取聊天列表
	 *
	 **/
//	@IgnoreAuth
	@GetMapping("getChatList")
	@ApiOperation(value="获取聊天列表", notes="获取聊天列表")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "token", value = "token", required = true,dataType = "string",paramType = "query"),
	})
	public R getChatList(@CurrentAccount @ApiIgnore AccountDto account){
		List<ChatFriendDto> chatFriendDtoList = chatFriendRepository.findByUserId(account.getId());
		return R.ok().put("data", chatFriendDtoList);
	}



}
