package com.esionet.controller.api;

import com.esionet.common.api.annotation.IgnoreAuth;
import com.esionet.common.utils.bean.R;
import com.esionet.entity.Cat;
import com.esionet.entity.dto.CatTreeDto;
import com.esionet.service.CatService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @Description: 微信小程序-小程序分类模块
 * @since chenxuan 2019-11-05 16:28:50
 */
@RestController
@RequestMapping("/api/cat")
@Api(tags = "微信小程序-小程序分类模块")
public class ApiCatController {
	@Autowired
	private CatService catService;

	/**
	 * 获取分类树
	 *
	 **/
	@IgnoreAuth
	@GetMapping("getCatTree")
	@ApiOperation(value="获取分类树", notes="获取分类树")
	@ApiImplicitParams({})
	public R getCatTree(){
		List<CatTreeDto> data = catService.getCatTree();
		return R.ok().put("data", data);
	}

}
