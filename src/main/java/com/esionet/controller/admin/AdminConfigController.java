package com.esionet.controller.admin;

import java.util.List;
import java.util.Map;
import java.util.Date;
import com.esionet.common.api.annotation.RequireAuth;
import com.esionet.common.enumresource.StatusEnum;
import com.esionet.common.annotation.SysLog;
import com.esionet.common.enumresource.WhetherEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import io.swagger.annotations.*;
import com.esionet.entity.SysConfig;
import com.esionet.service.SysConfigService;
import com.esionet.common.utils.bean.Query;
import com.esionet.common.utils.bean.R;

/**
 * @Description: 后台管理-参数模块
 * @since chenxuan 2019-10-13 09:29:10
 */
@RestController
@RequestMapping("/admin/config")
@Api(tags = "后台管理-参数模块")
public class AdminConfigController {

	@Autowired
	private SysConfigService configService;


	/**
	 * @Decription: 查询字典列表
	 * @param: params 查询参数
	 * @return: data列表数据 total列表总数
	 * @since: chenxuan 2019-10-13 09:29:10
	 **/
    @ApiOperation(value="获取参数列表", notes="获取参数列表")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "token", value = "token", required = true,dataType = "string",paramType = "query"),
            @ApiImplicitParam(name = "当前", value = "page", required = true,dataType = "integer",paramType = "query"),
            @ApiImplicitParam(name = "每页显示几条", value = "limit", required = true,dataType = "integer",paramType = "query"),
    })
    @GetMapping("list")
    @RequireAuth("config:list")
	public R listData(@RequestParam Map<String, Object> params){
		//查询列表数据
        Query query = new Query(params);
                                                                                                                                                                        
		List<SysConfig> configList = configService.getList(query);
		int total = configService.getCount(query);
        return R.ok().put("data",configList).put("total",total);
	}

    /**
     * @Decription: 保存参数
     * @return: config 参数对象
     * @since: chenxuan 2019-10-13 09:29:10
     **/
    @ApiOperation(value="保存参数", notes="保存参数")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "token", value = "token", required = true,dataType = "string",paramType = "query"),
    })
    @SysLog("保存参数")
    @PostMapping("save")
    @RequireAuth("config:save")
	public R save(@RequestBody @ApiParam(name="参数对象",value="json格式") SysConfig config){
        int count =configService.getCount(config);
        if(count>0){
            return R.error("参数编码已存在");
        }
        config.setIsSys(WhetherEnum.NO.getValue());
        config.setStatus(StatusEnum.ENABLE.getValue());
		config.setCreateTime(new Date());
        configService.save(config);
		return R.ok();
	}

    /**
     * @Decription: 修改参数
     * @return: config 参数对象
     * @since: chenxuan 2019-10-13 09:29:10
     **/
    @ApiOperation(value="修改参数", notes="修改参数")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "token", value = "token", required = true,dataType = "string",paramType = "query"),
    })
    @SysLog("修改参数")
    @PostMapping("update")
    @RequireAuth("config:update")
	public R update(@RequestBody @ApiParam(name="参数对象",value="json格式") SysConfig config){
        SysConfig oldConfig=configService.get(config.getId());
        if(!oldConfig.getCode().equals(config.getCode())){
            int count =configService.getCount(config);
            if(count>0){
                return R.error("参数编码已存在");
            }
        }
		config.setUpdateTime(new Date());
        configService.update(config);
		return R.ok();
	}

    /**
     * @Decription: 删除参数
     * @return: ids 数组
     * @since: chenxuan 2019-10-13 09:29:10
     **/
    @ApiOperation(value="删除参数", notes="删除参数")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "token", value = "token", required = true,dataType = "string",paramType = "query"),
    })
    @SysLog("删除参数")
    @DeleteMapping("/delete")
    @RequireAuth("config:delete")
	public R delete(@RequestBody  @ApiParam(name="ids",value="数组格式") String[] ids){
		configService.deleteBatch(ids);
		
		return R.ok();
	}

}
