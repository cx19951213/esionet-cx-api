package com.esionet.controller.admin;

import java.util.List;
import java.util.Map;
import java.util.Date;
import com.esionet.common.enumresource.StatusEnum;
import com.esionet.common.annotation.SysLog;
import com.esionet.common.api.annotation.RequireAuth;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import io.swagger.annotations.*;
import com.esionet.entity.Account;
import com.esionet.service.AccountService;
import com.esionet.common.utils.bean.Query;
import com.esionet.common.utils.bean.R;
import springfox.documentation.annotations.ApiIgnore;

/**
 * @Description: 后台管理-用户信息模块
 * @since chenxuan 2020-03-09 15:44:49
 */
@RestController
@RequestMapping("/admin/account")
@Api(tags = "后台管理-用户信息模块")
public class AdminAccountController {
	@Autowired
	private AccountService accountService;


	/**
	 * @Decription: 查询用户信息列表
	 * @param: params 查询参数
	 * @return: data列表数据 total列表总数
	 * @since: chenxuan 2020-03-09 15:44:49
	 **/
    @ApiOperation(value="获取用户信息列表", notes="获取用户信息列表")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "token",value = "token",  required = true,dataType = "string",paramType = "query"),
            @ApiImplicitParam(name = "page",value = "当前",  required = true,dataType = "integer",paramType = "query"),
            @ApiImplicitParam(name = "limit",value = "每页显示几条", required = true,dataType = "integer",paramType = "query"),
    })
    @GetMapping("list")
    @RequireAuth("account:list")
	public R list(@RequestParam @ApiIgnore Map<String, Object> params){
		//查询列表数据
        Query query = new Query(params);
                                                                                                                                                                                                                                                                                                                                                                                                                                                                
		List<Account> accountList = accountService.getList(query);
		int total = accountService.getCount(query);
        return R.ok().put("data",accountList).put("total",total);
	}

    /**
     * @Decription: 保存用户信息
     * @return: account 用户信息对象
     * @since: chenxuan 2020-03-09 15:44:49
     **/
    @ApiOperation(value="保存用户信息", notes="保存用户信息")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "token", value = "token", required = true,dataType = "string",paramType = "query"),
    })
    @SysLog("保存用户信息")
    @PostMapping("save")
    @RequireAuth("account:save")
	public R save(@RequestBody @ApiParam(name="用户信息对象",value="json格式") Account account){
		account.setCreateTime(new Date());
                                                                                                                                                                                                                                                                                                                                                                                                                                                                
        accountService.save(account);
		return R.ok();
	}

    /**
     * @Decription: 修改用户信息
     * @return: account 用户信息对象
     * @since: chenxuan 2020-03-09 15:44:49
     **/
    @ApiOperation(value="修改用户信息", notes="修改用户信息")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "token", value = "token", required = true,dataType = "string",paramType = "query"),
    })
    @SysLog("修改用户信息")
    @PostMapping("update")
    @RequireAuth("account:update")
	public R update(@RequestBody @ApiParam(name="用户信息对象",value="json格式") Account account){
		account.setUpdateTime(new Date());
        accountService.update(account);
		return R.ok();
	}

    /**
     * @Decription: 启用用户信息
     * @return: ids 数组
     * @since: chenxuan 2020-03-09 15:44:49
     **/
    @ApiOperation(value="启用用户信息", notes="启用用户信息")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "token", value = "token", required = true,dataType = "string",paramType = "query"),
    })
    @SysLog("禁用用户信息")
    @PostMapping("/enable")
    @RequireAuth("account:enable")
    public R enable(@RequestBody @ApiParam(name="ids",value="数组格式") String[] ids){
        String statusValue=StatusEnum.ENABLE.getValue();
		accountService.updateStatus(ids,statusValue);
        return R.ok();
    }

    /**
     * @Decription: 禁用用户信息
     * @return: ids 数组
     * @since: chenxuan 2020-03-09 15:44:49
     **/
    @ApiOperation(value="禁用用户信息", notes="禁用用户信息")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "token", value = "token", required = true,dataType = "string",paramType = "query"),
    })
    @SysLog("禁用用户信息")
    @PostMapping("/limit")
    @RequireAuth("account:limit")
    public R limit(@RequestBody @ApiParam(name="ids",value="数组格式") String[] ids){
        String statusValue=StatusEnum.LIMIT.getValue();
		accountService.updateStatus(ids,statusValue);
        return R.ok();
    }

    /**
     * @Decription: 删除用户信息
     * @return: ids 数组
     * @since: chenxuan 2020-03-09 15:44:49
     **/
    @ApiOperation(value="删除用户信息", notes="删除用户信息")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "token", value = "token", required = true,dataType = "string",paramType = "query"),
    })
    @SysLog("删除用户信息")
    @DeleteMapping("/delete")
    @RequireAuth("account:delete")
	public R delete(@RequestBody  @ApiParam(name="ids",value="数组格式") String[] ids){
		accountService.deleteBatch(ids);
		
		return R.ok();
	}

}
