package com.esionet.controller.admin;

import java.util.*;

import com.esionet.common.api.annotation.CurrentSysUser;
import com.esionet.common.api.annotation.IgnoreAuth;
import com.esionet.common.api.annotation.RequireAuth;
import com.esionet.common.enumresource.StatusEnum;
import com.esionet.common.annotation.SysLog;
import com.esionet.common.utils.bean.TreeVo;
import com.esionet.entity.SysMenu;
import com.esionet.entity.SysUser;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.esionet.service.SysMenuService;
import com.esionet.common.utils.bean.Query;
import com.esionet.common.utils.bean.R;

/**
 * 后台管理-菜单模块
 *
 * @since chenxuan
 */
@RestController
@RequestMapping("/admin/menu")
@Api(tags = "后台管理-菜单模块")
public class AdminMenuController {

    @Autowired
    private SysMenuService sysMenuService;

    /**
     * @Decription: 查询字典列表
     * @param: params 查询参数
     * @return: data列表数据 total列表总数
     * @since: chenxuan 2019-09-26 14:37:29
     **/
    @ApiOperation(value = "获取菜单列表", notes = "获取菜单列表")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "token", value = "token", required = true, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "当前", value = "page", required = true, dataType = "integer", paramType = "query"),
            @ApiImplicitParam(name = "每页显示几条", value = "limit", required = true, dataType = "integer", paramType = "query"),
    })
    @GetMapping("list")
    @RequireAuth("menu:list")
    public R list(@RequestParam Map<String, Object> params) {
        //查询列表数据
        Query query = new Query(params);
        List<SysMenu> sysMenuList = sysMenuService.getList(query);
        int total = sysMenuService.getCount(query);
        return R.ok().put("data", sysMenuList).put("total", total);
    }

    /**
     * @Decription: 保存菜单
     * @return: sysMenu 菜单对象
     * @since: chenxuan 2019-09-26 14:37:29
     **/
    @ApiOperation(value = "保存菜单", notes = "保存菜单")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "token", value = "token", required = true, dataType = "string", paramType = "query"),
    })
    @SysLog("保存菜单")
    @PostMapping("save")
    @RequireAuth("menu:save")
    public R save(@RequestBody @ApiParam(name = "菜单对象", value = "json格式") SysMenu sysMenu) {
        sysMenuService.save(sysMenu);
        return R.ok();
    }

    /**
     * @Decription: 修改菜单
     * @return: sysMenu 菜单对象
     * @since: chenxuan 2019-09-26 14:37:29
     **/
    @ApiOperation(value = "修改菜单", notes = "修改菜单")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "token", value = "token", required = true, dataType = "string", paramType = "query"),
    })
    @SysLog("修改菜单")
    @PostMapping("update")
    @RequireAuth("menu:update")
    public R update(@RequestBody @ApiParam(name = "菜单对象", value = "json格式") SysMenu sysMenu) {
        sysMenuService.update(sysMenu);
        return R.ok();
    }

    /**
     * @Decription: 启用菜单
     * @return: ids 数组
     * @since: chenxuan 2019-09-26 14:37:29
     **/
    @ApiOperation(value = "启用菜单", notes = "启用菜单")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "token", value = "token", required = true, dataType = "string", paramType = "query"),
    })
    @SysLog("启用菜单")
    @PostMapping("enable")
    @RequireAuth("menu:enable")
    public R enable(@RequestBody @ApiParam(name = "ids", value = "数组格式") String[] menuIds) {
        String statusValue = StatusEnum.ENABLE.getValue();
        sysMenuService.updateStatus(menuIds, statusValue);
        return R.ok();
    }

    /**
     * @Decription: 禁用菜单
     * @return: ids 数组
     * @since: chenxuan 2019-09-26 14:37:29
     **/
    @ApiOperation(value = "禁用菜单", notes = "禁用菜单")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "token", value = "token", required = true, dataType = "string", paramType = "query"),
    })
    @SysLog("禁用菜单")
    @PostMapping("/limit")
    @RequireAuth("menu:limit")
    public R limit(@RequestBody @ApiParam(name = "ids", value = "数组格式") String[] menuIds) {
        String statusValue = StatusEnum.LIMIT.getValue();
        sysMenuService.updateStatus(menuIds, statusValue);
        return R.ok();
    }

    /**
     * @Decription: 删除菜单
     * @return: ids 数组
     * @since: chenxuan 2019-09-26 14:37:29
     **/
    @ApiOperation(value = "删除菜单", notes = "删除菜单")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "token", value = "token", required = true, dataType = "string", paramType = "query"),
    })
    @SysLog("删除菜单")
    @DeleteMapping("/delete")
    @RequireAuth("menu:delete")
    public R delete(@RequestBody @ApiParam(name = "ids", value = "数组格式") String[] ids) {
        sysMenuService.deleteBatch(ids);

        return R.ok();
    }


    /**
     * @Decription: 获取用户菜单
     * @since: chenxuan 2019-09-26 14:37:29
     **/
    @ApiOperation(value = "获取菜单列表", notes = "获取菜单列表")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "token", value = "token", required = true, dataType = "string", paramType = "query"),
    })
    @GetMapping("getUserMenu")
    public R getUserMenu(@CurrentSysUser SysUser currentSysUser) {
        List<SysMenu> sysMenuList = sysMenuService.getUserMenuList(currentSysUser.getUserId());
        return R.ok().put("data", sysMenuList);
    }


    /**
     * @Decription: 获取菜单路由
     * @since: chenxuan 2019-09-26 14:37:29
     **/
    @ApiOperation(value = "获取菜单路由", notes = "获取菜单路由")
    @IgnoreAuth
    @GetMapping("getMenuRoute")
    public R getMenuRoute() {
        List<SysMenu> sysMenuList = sysMenuService.getMenuRoute();
        return R.ok().put("data", sysMenuList);
    }

    /**
     * @Decription: 获取菜单树
     * @since: chenxuan 2019-09-26 14:37:29
     **/
    @ApiOperation(value = "获取菜单树形列表", notes = "获取菜单树形列表")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "token", value = "token", required = true, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "是否包含按钮", value = "includeBtn", required = false, dataType = "boolean", paramType = "query"),
    })
    @GetMapping("treeData")
    public R treeData(boolean includeBtn) {
        List<TreeVo> data = sysMenuService.getTreeData(includeBtn);
        return R.ok().put("data", data);
    }

}
