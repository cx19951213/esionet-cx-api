package com.esionet.controller.admin;

import java.util.List;
import java.util.Map;
import java.util.Date;

import com.esionet.common.api.annotation.RequireAuth;
import com.esionet.common.enumresource.StatusEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import io.swagger.annotations.*;
import com.esionet.entity.SysLog;
import com.esionet.service.SysLogService;
import com.esionet.common.utils.bean.Query;
import com.esionet.common.utils.bean.R;

/**
 * @Description: 后台管理-操作日志模块
 * @since chenxuan 2019-10-13 09:29:10
 */
@RestController
@RequestMapping("/admin/log")
@Api(tags = "后台管理-操作日志模块")
public class AdminLogController {

    @Autowired
    private SysLogService logService;


    /**
     * @Decription: 查询日志列表
     * @param: params 查询参数
     * @return: data列表数据 total列表总数
     * @since: chenxuan 2019-10-13 09:29:10
     **/
    @ApiOperation(value = "获取操作日志列表", notes = "获取操作日志列表")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "token", value = "token", required = true, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "当前", value = "page", required = true, dataType = "integer", paramType = "query"),
            @ApiImplicitParam(name = "每页显示几条", value = "limit", required = true, dataType = "integer", paramType = "query"),
    })
    @GetMapping("list")
    @RequireAuth("log:list")
    public R list(@RequestParam Map<String, Object> params) {
        //查询列表数据
        Query query = new Query(params);
        List<SysLog> logList = logService.getList(query);
        int total = logService.getCount(query);
        return R.ok().put("data", logList).put("total", total);
    }

}
