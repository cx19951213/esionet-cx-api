package com.esionet.controller.admin;

import com.esionet.common.annotation.SysLog;
import com.esionet.common.api.annotation.RequireAuth;
import com.esionet.common.enumresource.StatusEnum;
import com.esionet.common.enumresource.WhetherEnum;
import com.esionet.common.utils.bean.Query;
import com.esionet.common.utils.bean.R;
import com.esionet.common.utils.bean.TreeVo;
import com.esionet.entity.Cat;
import com.esionet.service.CatService;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @Description: 后台管理-商品分类模块
 * @since chenxuan 2019-10-29 17:09:14
 */
@RestController
@RequestMapping("/admin/cat")
@Api(tags = "后台管理-商品分类模块")
public class AdminCatController {
	@Autowired
	private CatService catService;


	/**
	 * @Decription: 查询商品分类列表
	 * @param: params 查询参数
	 * @return: data列表数据 total列表总数
	 * @since: chenxuan 2019-10-29 17:09:14
	 **/
    @ApiOperation(value="获取商品分类列表", notes="获取商品分类列表")
    @ApiImplicitParams({
            @ApiImplicitParam(value = "token", name = "token", required = true,dataType = "string",paramType = "query"),
            @ApiImplicitParam(value = "当前", name = "page", required = true,dataType = "integer",paramType = "query"),
            @ApiImplicitParam(value = "每页显示几条", name = "limit", required = true,dataType = "integer",paramType = "query"),
    })
    @GetMapping("list")
    @RequireAuth("cat:list")
	public R list(@RequestParam @ApiIgnore Map<String, Object> params){
		//查询列表数据
        Query query = new Query(params);
        query.put("isDel", WhetherEnum.NO.getValue());
		List<Cat> catList = catService.getList(query);
		int total = catService.getCount(query);
        return R.ok().put("data",catList).put("total",total);
	}

    /**
     * @Decription: 保存商品分类
     * @return: cat 商品分类对象
     * @since: chenxuan 2019-10-29 17:09:14
     **/
    @ApiOperation(value="保存商品分类", notes="保存商品分类")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "token", value = "token", required = true,dataType = "string",paramType = "query"),
    })
    @SysLog("保存商品分类")
    @PostMapping("save")
    @RequireAuth("cat:save")
	public R save(@RequestBody @ApiParam(name="商品分类对象",value="json格式") Cat cat){
		cat.setCreateTime(new Date());
        cat.setIsDel(WhetherEnum.NO.getValue());
        cat.setIsRecommend(WhetherEnum.NO.getValue());
        catService.save(cat);
		return R.ok();
	}

    /**
     * @Decription: 修改商品分类
     * @return: cat 商品分类对象
     * @since: chenxuan 2019-10-29 17:09:14
     **/
    @ApiOperation(value="修改商品分类", notes="修改商品分类")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "token", value = "token", required = true,dataType = "string",paramType = "query"),
    })
    @SysLog("修改商品分类")
    @PostMapping("update")
    @RequireAuth("cat:update")
	public R update(@RequestBody @ApiParam(name="商品分类对象",value="json格式") Cat cat){
		cat.setUpdateTime(new Date());
        catService.update(cat);
		return R.ok();
	}

    /**
     * @Decription: 启用商品分类
     * @return: ids 数组
     * @since: chenxuan 2019-10-29 17:09:14
     **/
    @ApiOperation(value="启用商品分类", notes="启用商品分类")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "token", value = "token", required = true,dataType = "string",paramType = "query"),
    })
    @SysLog("禁用商品分类")
    @PostMapping("/enable")
    @RequireAuth("cat:enable")
    public R enable(@RequestBody @ApiParam(name="ids",value="数组格式") String[] ids){
        String statusValue= StatusEnum.ENABLE.getValue();
		catService.updateStatus(ids,statusValue);
        return R.ok();
    }


    /**
     * @Decription: 禁用商品分类
     * @return: ids 数组
     * @since: chenxuan 2019-10-29 17:09:14
     **/
    @ApiOperation(value="禁用商品分类", notes="禁用商品分类")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "token", value = "token", required = true,dataType = "string",paramType = "query"),
    })
    @SysLog("禁用商品分类")
    @PostMapping("/limit")
    @RequireAuth("cat:limit")
    public R limit(@RequestBody @ApiParam(name="ids",value="数组格式") String[] ids){
        String statusValue= StatusEnum.LIMIT.getValue();
		catService.updateStatus(ids,statusValue);
        return R.ok();
    }

    /**
     * @Decription: 删除商品分类
     * @return: ids 数组
     * @since: chenxuan 2019-10-29 17:09:14
     **/
    @ApiOperation(value="删除商品分类", notes="删除商品分类")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "token", value = "token", required = true,dataType = "string",paramType = "query"),
    })
    @SysLog("删除商品分类")
    @DeleteMapping("/delete")
    @RequireAuth("cat:delete")
	public R delete(@RequestBody  @ApiParam(name="ids",value="数组格式") String[] ids){
		catService.deleteBatch(ids);
		
		return R.ok();
	}

    /**
     * @Decription: 获取分类树
     * @since: chenxuan 2019-09-26 14:37:29
     **/
    @ApiOperation(value = "获取分类树形列表", notes = "获取分类树形列表")
    @ApiImplicitParams({
            @ApiImplicitParam(value = "token", name = "token", required = true, dataType = "string", paramType = "query"),
            @ApiImplicitParam(value = "是否三级分类", name = "includeThree", required = false, dataType = "boolean", paramType = "query"),
    })
    @GetMapping("treeData")
    public R treeData(boolean includeThree) {
        List<TreeVo> data = catService.getTreeData(includeThree);
        return R.ok().put("data", data);
    }
}
