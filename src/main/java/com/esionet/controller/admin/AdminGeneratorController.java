package com.esionet.controller.admin;

import com.esionet.common.api.annotation.IgnoreAuth;
import com.esionet.common.utils.Underline2Camel;
import com.esionet.common.utils.bean.Query;
import com.esionet.common.utils.bean.R;
import com.esionet.common.xss.XssHttpServletRequestWrapper;
import com.esionet.service.SysGeneratorService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 * 后台管理-代码生成器模块
 * @author chenxuan
 */
@Controller
@RequestMapping("/admin/generator")
@Api(tags = "后台管理-代码生成器模块")
public class AdminGeneratorController {

	@Autowired
	private SysGeneratorService sysGeneratorService;

	/**
	 * 列表
	 */
	@ResponseBody
	@GetMapping("/list")
	@ApiOperation(value="获取数据库表列表", notes="获取数据库表列表")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "token", value = "token", required = true,dataType = "string",paramType = "query"),
			@ApiImplicitParam(name = "当前", value = "page", required = true,dataType = "integer",paramType = "query"),
			@ApiImplicitParam(name = "每页显示几条", value = "limit", required = true,dataType = "integer",paramType = "query"),
			@ApiImplicitParam(name = "表名", value = "tableName", required = false,dataType = "string",paramType = "query"),
			@ApiImplicitParam(name = "表备注", value = "tableComment", required = false,dataType = "string",paramType = "query"),
	})
	public R list(@RequestParam Map<String, Object> params){
		//查询列表数据
		Query query = new Query(params);
		List<Map<String, Object>> list = sysGeneratorService.getList(query);
		int total = sysGeneratorService.getCount(query);

		return R.ok().put("data", list).put("total",total);
	}



	/**
	 * 获取表
	 */
	@ResponseBody
	@GetMapping("/selectTables")
	@ApiOperation(value="获取表", notes="获取字段")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "token", value = "token", required = true,dataType = "string",paramType = "query"),
	})
	public R selectTables(){
		//查询列表数据
		List<Map<String, String>>  list = sysGeneratorService.selectTables();
		return R.ok().put("data", list);
	}

	/**
	 * 获取字段
	 */
	@ResponseBody
	@GetMapping("/getColumns")
	@ApiOperation(value="获取字段", notes="获取字段")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "token", value = "token", required = true,dataType = "string",paramType = "query"),
			@ApiImplicitParam(name = "表名", value = "tableName", required = true,dataType = "string",paramType = "query"),
	})
	public R getColumns(@RequestParam String tableName){
		//查询列表数据
		List<Map<String, String>>  list = sysGeneratorService.getColumns(tableName);
		for(Map<String, String> column : list){
			if(!"datetime".equals(column.get("dataType"))){
				//设置字段长度
				int start=column.get("maxLength").indexOf("(");
				int end =column.get("maxLength").indexOf(")");
				if(start!=-1){
					column.put("maxLength",column.get("maxLength").substring(start+1,end));
				}else{
					column.put("maxLength","20");
				}
			}else{
				column.put("maxLength","0");
			}
			column.put("columnName", Underline2Camel.underline2Camel(column.get("columnName"),true));
		}
		return R.ok().put("data", list);
	}


	/**
	 * 生成代码
	 */
	@GetMapping("/code")
	@IgnoreAuth
	public void code(HttpServletRequest request, HttpServletResponse response) throws IOException{
		//获取表名，不进行xss过滤
		HttpServletRequest orgRequest = XssHttpServletRequestWrapper.getOrgRequest(request);
		String[] tableNames = orgRequest.getParameter("tableNames").split(",");
		
		byte[] data = sysGeneratorService.generatorCode(tableNames);
		
		response.reset();  
        response.setHeader("Content-Disposition", "attachment; filename=\"code.zip\"");
        response.addHeader("Content-Length", "" + data.length);  
        response.setContentType("application/octet-stream; charset=UTF-8");  
  
        IOUtils.write(data, response.getOutputStream());  
	}
}
