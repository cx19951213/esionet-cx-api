package com.esionet.controller.admin;

import java.util.List;
import java.util.Map;

import com.esionet.common.api.annotation.CurrentSysUser;
import com.esionet.common.api.annotation.RequireAuth;
import com.esionet.common.enumresource.StatusEnum;
import com.esionet.common.annotation.SysLog;
import com.esionet.common.enumresource.WhetherEnum;
import com.esionet.entity.SysRole;
import com.esionet.entity.SysUser;
import com.esionet.service.SysRoleService;
import com.esionet.service.SysUserService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import io.swagger.annotations.*;
import com.esionet.common.utils.bean.Query;
import com.esionet.common.utils.bean.R;

/**
 * @Description: 管理员
 * @since chenxuan 2019-10-10 20:47:39
 */
@RestController
@RequestMapping("/admin/user")
@Api(tags = "后台管理-管理员模块")
public class AdminUserController {

    @Autowired
    private SysUserService userService;
    @Autowired
    private SysRoleService roleService;

    /**
     * @Decription: 查询字典列表
     * @param: params 查询参数
     * @return: data列表数据 total列表总数
     * @since: chenxuan 2019-10-10 20:47:39
     **/
    @ApiOperation(value = "获取管理员列表", notes = "获取管理员列表")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "token", value = "token", required = true, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "当前", value = "page", required = true, dataType = "integer", paramType = "query"),
            @ApiImplicitParam(name = "每页显示几条", value = "limit", required = true, dataType = "integer", paramType = "query"),
    })
    @GetMapping("list")
    @RequireAuth("user:list")
    public R list(@RequestParam Map<String, Object> params) {
        //查询列表数据
        Query query = new Query(params);

        List<SysUser> userList = userService.getList(query);
        for (SysUser user : userList) {
            String roleId = user.getRoleId();
            StringBuilder sb = new StringBuilder();
            if (!StringUtils.isEmpty(roleId)) {
                String[] roleIds = roleId.split(",");
                user.setRoleIds(roleIds);
                for (String id : roleIds) {
                    SysRole role = roleService.get(id);
                    if (role != null) {
                        sb.append(role.getRoleName()).append(" ");
                    }
                }
            }
            user.setRoleId(sb.toString());

        }
        int total = userService.getCount(query);
        return R.ok().put("data", userList).put("total", total);
    }

    /**
     * @Decription: 保存管理员
     * @return: user 管理员对象
     * @since: chenxuan 2019-10-10 20:47:39
     **/
    @ApiOperation(value = "保存管理员", notes = "保存管理员")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "token", value = "token", required = true, dataType = "string", paramType = "query"),
    })
    @SysLog("保存管理员")
    @PostMapping("save")
    @RequireAuth("user:save")
    public R save(@RequestBody @ApiParam(name = "管理员对象", value = "json格式") SysUser user, @CurrentSysUser SysUser currentUser) {
        user.setCreateUserId(currentUser.getUserId());
        user.setIsSys(WhetherEnum.NO.getValue());
        user.setStatus(WhetherEnum.YES.getValue());
        userService.save(user);
        return R.ok();
    }

    /**
     * @Decription: 修改管理员
     * @return: user 管理员对象
     * @since: chenxuan 2019-10-10 20:47:39
     **/
    @ApiOperation(value = "修改管理员", notes = "修改管理员")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "token", value = "token", required = true, dataType = "string", paramType = "query"),
    })
    @SysLog("修改管理员")
    @PostMapping("update")
    @RequireAuth("user:update")
    public R update(@RequestBody @ApiParam(name = "管理员对象", value = "json格式") SysUser user) {
        userService.update(user);
        return R.ok();
    }

    /**
     * @Decription: 启用管理员
     * @return: ids 数组
     * @since: chenxuan 2019-10-10 20:47:39
     **/
    @ApiOperation(value = "启用管理员", notes = "启用管理员")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "token", value = "token", required = true, dataType = "string", paramType = "query"),
    })
    @SysLog("启用管理员")
    @PostMapping("enable")
    @RequireAuth("user:update")
    public R enable(@RequestBody @ApiParam(name = "userIds", value = "数组格式") String[] ids) {
        String statusValue = StatusEnum.ENABLE.getValue();
        userService.updateStatus(ids, statusValue);
        return R.ok();
    }

    /**
     * @Decription: 禁用管理员
     * @return: ids 数组
     * @since: chenxuan 2019-10-10 20:47:39
     **/
    @ApiOperation(value = "禁用管理员", notes = "禁用管理员")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "token", value = "token", required = true, dataType = "string", paramType = "query"),
    })
    @SysLog("禁用管理员")
    @PostMapping("/limit")
    @RequireAuth("user:update")
    public R limit(@RequestBody @ApiParam(name = "userIds", value = "数组格式") String[] ids) {
        String statusValue = StatusEnum.LIMIT.getValue();
        userService.updateStatus(ids, statusValue);
        return R.ok();
    }

    /**
     * @Decription: 删除管理员
     * @return: ids 数组
     * @since: chenxuan 2019-10-10 20:47:39
     **/
    @ApiOperation(value = "删除管理员", notes = "删除管理员")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "token", value = "token", required = true, dataType = "string", paramType = "query"),
    })
    @SysLog("删除管理员")
    @DeleteMapping("/delete")
    @RequireAuth("user:delete")
    public R delete(@RequestBody @ApiParam(name = "ids", value = "数组格式") String[] ids) {
        userService.deleteBatch(ids);

        return R.ok();
    }


    /**
     * 初始化密码
     */
    @ApiOperation(value = "初始化管理员密码", notes = "初始化管理员密码")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "token", value = "token", required = true, dataType = "string", paramType = "query"),
    })
    @SysLog("初始化密码")
    @PostMapping("/initPassword")
    @RequireAuth("user:initPassword")
    public R initPassword(@RequestBody @ApiParam(name = "ids", value = "数组格式") String[] ids) {
        userService.initPassword(ids);
        return R.ok();
    }


}
