package com.esionet.controller.admin;

import com.esionet.common.annotation.SysLog;
import com.esionet.common.enumresource.WhetherEnum;
import com.esionet.common.utils.bean.Query;
import com.esionet.common.utils.bean.R;
import com.esionet.common.utils.bean.TreeVo;
import com.esionet.entity.SysFile;
import com.esionet.service.SysFileService;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import springfox.documentation.annotations.ApiIgnore;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @Description: 后台管理-文件模块
 * @since chenxuan 2019-10-13 11:47:54
 */
@RestController
@RequestMapping("/admin/file")
@Api(tags = "后台管理-文件模块")
public class AdminFileController {

	@Autowired
	private SysFileService fileService;


	/**
	 * @Decription: 查询文件列表
	 * @param: params 查询参数
	 * @return: data列表数据 total列表总数
	 * @since: chenxuan 2019-10-13 11:47:54
	 **/
    @ApiOperation(value="获取文件列表", notes="获取文件列表")
    @ApiImplicitParams({
            @ApiImplicitParam(value = "token", name = "token", required = true,dataType = "string",paramType = "query"),
            @ApiImplicitParam(value = "当前", name = "page", required = true,dataType = "integer",paramType = "query"),
            @ApiImplicitParam(value = "每页显示几条", name = "limit", required = true,dataType = "integer",paramType = "query"),
    })
    @GetMapping("list")
	public R listData(@RequestParam @ApiIgnore Map<String, Object> params){
		//查询列表数据
        Query query = new Query(params);
        query.put("isDel", WhetherEnum.NO.getValue());
		List<SysFile> fileList = fileService.getList(query);
		int total = fileService.getCount(query);
        return R.ok().put("data",fileList).put("total",total);
	}

	/**
	 * @Decription: 查询文件列表
	 * @param: params 查询参数
	 * @return: data列表数据 total列表总数
	 * @since: chenxuan 2019-10-13 11:47:54
	 **/
    @ApiOperation(value="获取文件列表", notes="获取文件列表")
    @ApiImplicitParams({
            @ApiImplicitParam(value = "token", name = "token", required = true,dataType = "string",paramType = "query"),
            @ApiImplicitParam(value = "当前", name = "page", required = true,dataType = "integer",paramType = "query"),
            @ApiImplicitParam(value = "每页显示几条", name = "limit", required = true,dataType = "integer",paramType = "query"),
    })
    @GetMapping("getRecycleList")
	public R getRecycleList(@RequestParam @ApiIgnore Map<String, Object> params){
		//查询列表数据
        Query query = new Query(params);
		List<SysFile> fileList = fileService.getRecycleList(query);
		int total = fileService.getCount(query);
        return R.ok().put("data",fileList).put("total",total);
	}

    /**
     * @Decription: 删除文件
     * @return: ids 数组
     * @since: chenxuan 2019-10-13 11:47:54
     **/
    @ApiOperation(value="删除文件", notes="删除文件")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "token", value = "token", required = true,dataType = "string",paramType = "query"),
    })
    @SysLog("删除文件")
    @DeleteMapping("/delete")
	public R delete(@RequestBody  @ApiParam(name="ids",value="数组格式") String[] ids){
		fileService.deleteBatch(ids);
		
		return R.ok();
	}

    @ApiOperation(value="新建文件夹", notes="新建文件夹")
    @ApiImplicitParams({
            @ApiImplicitParam(value = "token", name = "token", required = true,dataType = "string",paramType = "query"),
            @ApiImplicitParam(value = "上级文件夹ID", name = "parentId", required = true,dataType = "string",paramType = "query"),
            @ApiImplicitParam(value = "文件夹名称", name = "fileName", required = true,dataType = "string",paramType = "query"),
    })
    @SysLog("新建文件夹")
    @PostMapping("/addFolder")
    public R addFolder(@RequestBody SysFile file){
        return fileService.addFolder(file.getParentId(),file.getFileName());
    }

    /**
     * 添加文件到回收站
     */
    @ApiOperation(value="添加文件到回收站", notes="添加文件到回收站")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "token", value = "token", required = true,dataType = "string",paramType = "query"),
    })
    @SysLog("添加文件到回收站")
    @PostMapping("/addToRecycle")
    public R addToRecycle(@RequestBody  @ApiParam(name="ids",value="数组格式")  String[] ids){
        fileService.addToRecycle(ids);
        return R.ok();
    }
    /**
     * 清空回收站
     */
    @ApiOperation(value="清空回收站", notes="清空回收站")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "token", value = "token", required = true,dataType = "string",paramType = "query"),
    })
    @SysLog("清空回收站")
    @DeleteMapping("/clearRecycle")
    public R clearRecycle(){
        fileService.clearRecycle();
        return R.ok();
    }


    /**
     * 从回收站中还原
     */
    @ApiOperation(value="文件还原", notes="文件还原")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "token", value = "token", required = true,dataType = "string",paramType = "query"),
    })
    @SysLog("文件还原")
    @PostMapping("/restoreFile")
    public R restoreFile(@RequestBody  @ApiParam(name="ids",value="数组格式")  String[] ids){
        fileService.restoreFile(ids);
        return R.ok();
    }


    /**
     * 文件重命名
     */
    @ApiOperation(value="文件重命名", notes="文件重命名")
    @ApiImplicitParams({
            @ApiImplicitParam(value = "token", name = "token", required = true,dataType = "string",paramType = "query"),
            @ApiImplicitParam(value = "文件ID", name = "fileId", required = true,dataType = "string",paramType = "query"),
            @ApiImplicitParam(value = "文件名称", name = "newFileName", required = true,dataType = "string",paramType = "query"),
    })
    @SysLog("文件重命名")
    @PostMapping("/rename")
    public R rename(@RequestBody Map<String,Object> params){
        String fileId = String.valueOf(params.get("fileId"));
        String newFileName = String.valueOf(params.get("newFileName"));
        fileService.rename(fileId,newFileName);
        return R.ok();
    }

    /**
     * 获取文件夹目录
     */
    @ApiOperation(value="获取文件夹目录", notes="获取文件夹目录")
    @ApiImplicitParams({
            @ApiImplicitParam(value = "token", name = "token", required = true,dataType = "string",paramType = "query"),
            @ApiImplicitParam(value = "文件ID", name = "fileId", required = true,dataType = "string",paramType = "query"),
    })
    @GetMapping("/selectFolders")
    public R selectFolders(String fileId){
        List<TreeVo> folderList = fileService.selectFolders(fileId);
        TreeVo top = new TreeVo();
        top.setLabel("全部文件");
        top.setValue("0");
        top.setChildren(folderList);
        List<TreeVo> data = new ArrayList<>();
        data.add(top);
        return R.ok().put("data",data);
    }


    /**
     * 移动文件
     */
    @ApiOperation(value="移动文件", notes="移动文件")
    @ApiImplicitParams({
            @ApiImplicitParam(value = "token", name = "token", required = true,dataType = "string",paramType = "query"),
            @ApiImplicitParam(value = "文件ID", name = "fileId", required = true,dataType = "string",paramType = "query"),
            @ApiImplicitParam(value = "上级文件ID", name = "parentId", required = true,dataType = "string",paramType = "query"),
    })
    @SysLog("移动文件")
    @PostMapping("/moveFile")
    public R moveFile(@RequestBody Map<String,Object> params){
        String fileId = String.valueOf(params.get("fileId"));
        String parentId = String.valueOf(params.get("parentId"));
        return fileService.moveFile(fileId,parentId);
    }



    @ApiOperation(value = "上传文件", notes = "上传文件")
    @ApiImplicitParams({
            @ApiImplicitParam(value = "token", name = "token", required = true, dataType = "string", paramType = "query"),
            @ApiImplicitParam(value = "上级ID", name = "parentId", required = true, dataType = "string", paramType = "query"),
    })
    @PostMapping("/upload")
    public R upload(@RequestParam("file") MultipartFile file, String parentId) {
        return fileService.upload(file,parentId);
    }


    /**
     * @Decription: 查询文件夹与图片
     * @param: params 查询参数
     * @return: data列表数据 total列表总数
     * @since: chenxuan 2019-10-13 11:47:54
     **/
    @ApiOperation(value="获取文件列表", notes="获取文件列表")
    @ApiImplicitParams({
            @ApiImplicitParam(value = "token", name = "token", required = true,dataType = "string",paramType = "query"),
            @ApiImplicitParam(value = "当前", name = "page", required = true,dataType = "integer",paramType = "query"),
            @ApiImplicitParam(value = "每页显示几条", name = "limit", required = true,dataType = "integer",paramType = "query"),
            @ApiImplicitParam(value = "文件名", name = "fileNameVague", required = true,dataType = "string",paramType = "query"),
            @ApiImplicitParam(value = "上级文件夹ID", name = "parentId", required = true,dataType = "string",paramType = "query"),
    })
    @GetMapping("getImgList")
    public R getImgList(@RequestParam @ApiIgnore Map<String, Object> params){
        //查询列表数据
        Query query = new Query(params);
        List<SysFile> fileList = fileService.getImgList(query);
        int total = fileService.getCount(query);
        return R.ok().put("data",fileList).put("total",total);
    }
}
