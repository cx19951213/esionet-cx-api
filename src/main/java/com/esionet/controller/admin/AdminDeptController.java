package com.esionet.controller.admin;

import java.util.*;

import com.esionet.common.api.annotation.RequireAuth;
import com.esionet.common.enumresource.StatusEnum;
import com.esionet.common.annotation.SysLog;
import com.esionet.common.enumresource.WhetherEnum;
import com.esionet.common.utils.bean.TreeVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import io.swagger.annotations.*;
import com.esionet.entity.SysDept;
import com.esionet.service.SysDeptService;
import com.esionet.common.utils.bean.Query;
import com.esionet.common.utils.bean.R;

/**
 * @Description: 后台管理-部门模块
 * @since chenxuan 2019-10-12 22:13:52
 */
@RestController
@RequestMapping("/admin/dept")
@Api(tags = "后台管理-部门模块")
public class AdminDeptController {

	@Autowired
	private SysDeptService deptService;


	/**
	 * @Decription: 查询字典列表
	 * @param: params 查询参数
	 * @return: data列表数据 total列表总数
	 * @since: chenxuan 2019-10-12 22:13:52
	 **/
    @ApiOperation(value="获取部门列表", notes="获取部门列表")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "token", value = "token", required = true,dataType = "string",paramType = "query"),
            @ApiImplicitParam(name = "当前", value = "page", required = true,dataType = "integer",paramType = "query"),
            @ApiImplicitParam(name = "每页显示几条", value = "limit", required = true,dataType = "integer",paramType = "query"),
    })
    @GetMapping("list")
    @RequireAuth("dept:list")
	public R listData(@RequestParam Map<String, Object> params){
		//查询列表数据
        Query query = new Query(params);
                                                                                                                                                                                                                
		List<SysDept> deptList = deptService.getList(query);
		int total = deptService.getCount(query);
        return R.ok().put("data",deptList).put("total",total);
	}

    /**
     * @Decription: 保存部门
     * @return: dept 部门对象
     * @since: chenxuan 2019-10-12 22:13:52
     **/
    @ApiOperation(value="保存部门", notes="保存部门")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "token", value = "token", required = true,dataType = "string",paramType = "query"),
    })
    @SysLog("保存部门")
    @PostMapping("save")
    @RequireAuth("dept:save")
	public R save(@RequestBody @ApiParam(name="部门对象",value="json格式") SysDept dept){
		dept.setCreateTime(new Date());
        dept.setIsSys(WhetherEnum.NO.getValue());
        deptService.save(dept);
		return R.ok();
	}

    /**
     * @Decription: 修改部门
     * @return: dept 部门对象
     * @since: chenxuan 2019-10-12 22:13:52
     **/
    @ApiOperation(value="修改部门", notes="修改部门")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "token", value = "token", required = true,dataType = "string",paramType = "query"),
    })
    @SysLog("修改部门")
    @PostMapping("update")
    @RequireAuth("dept:update")
	public R update(@RequestBody @ApiParam(name="部门对象",value="json格式") SysDept dept){
		dept.setUpdateTime(new Date());
        deptService.update(dept);
		return R.ok();
	}

    /**
     * @Decription: 删除部门
     * @return: ids 数组
     * @since: chenxuan 2019-10-12 22:13:52
     **/
    @ApiOperation(value="删除部门", notes="删除部门")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "token", value = "token", required = true,dataType = "string",paramType = "query"),
    })
    @SysLog("删除部门")
    @DeleteMapping("/delete")
    @RequireAuth("dept:delete")
	public R delete(@RequestBody  @ApiParam(name="ids",value="数组格式") String[] ids){
		deptService.deleteBatch(ids);
		
		return R.ok();
	}

    @ApiOperation(value="获取部门树", notes="获取部门树")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "token", value = "token", required = true,dataType = "string",paramType = "query"),
    })
    @SysLog("删除部门")
    @GetMapping("/treeData")
    public R treeData() {
         return deptService.treeData();
    }

}
