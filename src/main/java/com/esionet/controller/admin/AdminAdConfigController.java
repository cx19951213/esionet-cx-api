package com.esionet.controller.admin;

import com.esionet.common.annotation.SysLog;
import com.esionet.common.api.annotation.RequireAuth;
import com.esionet.common.enumresource.StatusEnum;
import com.esionet.common.enumresource.WhetherEnum;
import com.esionet.common.utils.bean.Query;
import com.esionet.common.utils.bean.R;
import com.esionet.entity.AdConfig;
import com.esionet.service.AdConfigService;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @Description: 后台管理-小程序广告配置模块
 * @since chenxuan 2019-11-05 16:28:50
 */
@RestController
@RequestMapping("/admin/adConfig")
@Api(tags = "后台管理-小程序广告配置模块")
public class AdminAdConfigController {
	@Autowired
	private AdConfigService adConfigService;


	/**
	 * @Decription: 查询小程序广告配置列表
	 * @param: params 查询参数
	 * @return: data列表数据 total列表总数
	 * @since: chenxuan 2019-11-05 16:28:50
	 **/
    @ApiOperation(value="获取小程序广告配置列表", notes="获取小程序广告配置列表")
    @ApiImplicitParams({
            @ApiImplicitParam(value = "token", name = "token", required = true,dataType = "string",paramType = "query"),
            @ApiImplicitParam(value = "当前", name = "page", required = true,dataType = "integer",paramType = "query"),
            @ApiImplicitParam(value = "每页显示几条", name = "limit", required = true,dataType = "integer",paramType = "query"),
    })
    @GetMapping("list")
    @RequireAuth("adConfig:list")
	public R list(@RequestParam @ApiIgnore Map<String, Object> params){
		//查询列表数据
        Query query = new Query(params);
        query.put("isDel", WhetherEnum.NO.getValue());
		List<AdConfig> adConfigList = adConfigService.getList(query);
		int total = adConfigService.getCount(query);
        return R.ok().put("data",adConfigList).put("total",total);
	}

    /**
     * @Decription: 保存小程序广告配置
     * @return: adConfig 小程序广告配置对象
     * @since: chenxuan 2019-11-05 16:28:50
     **/
    @ApiOperation(value="保存小程序广告配置", notes="保存小程序广告配置")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "token", value = "token", required = true,dataType = "string",paramType = "query"),
    })
    @SysLog("保存小程序广告配置")
    @PostMapping("save")
    @RequireAuth("adConfig:save")
	public R save(@RequestBody @ApiParam(name="小程序广告配置对象",value="json格式") AdConfig adConfig){
		adConfig.setCreateTime(new Date());
        adConfig.setIsDel(WhetherEnum.NO.getValue());
        adConfigService.save(adConfig);
		return R.ok();
	}

    /**
     * @Decription: 修改小程序广告配置
     * @return: adConfig 小程序广告配置对象
     * @since: chenxuan 2019-11-05 16:28:50
     **/
    @ApiOperation(value="修改小程序广告配置", notes="修改小程序广告配置")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "token", value = "token", required = true,dataType = "string",paramType = "query"),
    })
    @SysLog("修改小程序广告配置")
    @PostMapping("update")
    @RequireAuth("adConfig:update")
	public R update(@RequestBody @ApiParam(name="小程序广告配置对象",value="json格式") AdConfig adConfig){
		adConfig.setUpdateTime(new Date());
        adConfigService.update(adConfig);
		return R.ok();
	}

    /**
     * @Decription: 启用小程序广告配置
     * @return: ids 数组
     * @since: chenxuan 2019-11-05 16:28:50
     **/
    @ApiOperation(value="启用小程序广告配置", notes="启用小程序广告配置")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "token", value = "token", required = true,dataType = "string",paramType = "query"),
    })
    @SysLog("禁用小程序广告配置")
    @PostMapping("/enable")
    @RequireAuth("adConfig:enable")
    public R enable(@RequestBody @ApiParam(name="ids",value="数组格式") String[] ids){
        String statusValue= StatusEnum.ENABLE.getValue();
		adConfigService.updateStatus(ids,statusValue);
        return R.ok();
    }

    /**
     * @Decription: 禁用小程序广告配置
     * @return: ids 数组
     * @since: chenxuan 2019-11-05 16:28:50
     **/
    @ApiOperation(value="禁用小程序广告配置", notes="禁用小程序广告配置")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "token", value = "token", required = true,dataType = "string",paramType = "query"),
    })
    @SysLog("禁用小程序广告配置")
    @PostMapping("/limit")
    @RequireAuth("adConfig:limit")
    public R limit(@RequestBody @ApiParam(name="ids",value="数组格式") String[] ids){
        String statusValue= StatusEnum.LIMIT.getValue();
		adConfigService.updateStatus(ids,statusValue);
        return R.ok();
    }

    /**
     * @Decription: 删除小程序广告配置
     * @return: ids 数组
     * @since: chenxuan 2019-11-05 16:28:50
     **/
    @ApiOperation(value="删除小程序广告配置", notes="删除小程序广告配置")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "token", value = "token", required = true,dataType = "string",paramType = "query"),
    })
    @SysLog("删除小程序广告配置")
    @DeleteMapping("/delete")
    @RequireAuth("adConfig:delete")
	public R delete(@RequestBody  @ApiParam(name="ids",value="数组格式") String[] ids){
		adConfigService.deleteBatch(ids);
		
		return R.ok();
	}

}
