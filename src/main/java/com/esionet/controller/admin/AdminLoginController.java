package com.esionet.controller.admin;

import com.esionet.common.api.annotation.IgnoreAuth;
import com.esionet.common.utils.bean.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.esionet.service.SysUserService;


/**
 * 后台管理-登陆模块
 *
 * @author chenxuan
 */
@RestController
@RequestMapping("/admin/")
@Api(tags = "后台管理-登陆模块")
public class AdminLoginController {

    @Autowired
    private SysUserService userService;

    /**
     * 登录
     */

    @IgnoreAuth
    @GetMapping("login")
    @ApiOperation(value = "用户登陆", notes = "用户登陆")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "username", value = "用户名", required = true, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "password", value = "密码", required = true, dataType = "string", paramType = "query"),
    })
    public R login(String username, String password) {
        if (StringUtils.isBlank(username)) {
            return R.error("账号不能为空");
        } else if (StringUtils.isBlank(password)) {
            return R.error("密码不能为空");
        }
        //生成token
        return userService.login(username, password);
    }


}
