package com.esionet.controller.admin;

import com.esionet.common.exception.MyException;
import com.esionet.common.utils.bean.EnumBean;
import com.esionet.common.utils.bean.EnumMessage;
import com.esionet.common.utils.bean.R;
import com.esionet.common.utils.constants.Constant;
import com.esionet.service.SysDictService;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 后台管理-通用模块
 *
 * @author chenxuan
 */
@RestController
@RequestMapping("/admin/common")
@Api(tags = "后台管理-通用模块")
public class AdminCommonController {

    @Autowired
    private SysDictService sysDictService;

    /**
     * @param
     * @author chenxuan
     * @Description 通过枚举获取数据列表
     * @date date 2017-7-20
     */
    @GetMapping("/getEnum")
    @ApiOperation(value = "通过枚举获取数据列表", notes = "通过枚举获取数据列表")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "enumName", value = "枚举名称", required = true, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "token", value = "token", required = true, dataType = "string", paramType = "query"),
    })
    public R getEnum(String enumName) throws Exception {
        List<EnumBean> values = new ArrayList<>();
        if (enumName != null && !"".equals(enumName)) {
            Class clzz = null;
            try {
                clzz = Class.forName(Constant.PACKAGE_NAME + "." + enumName);
                Method method = clzz.getMethod("values");
                EnumMessage inter[] = (EnumMessage[]) method.invoke(new Object[]{}, new Object[]{});
                for (EnumMessage enumMessage : inter) {
                    EnumBean e = new EnumBean();
                    e.setValue(enumMessage.getValue());
                    e.setLabel(enumMessage.getLabel());
                    values.add(e);
                }
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
        }
        return R.ok().put("data", values);
    }


    /**
     * @param
     * @author chenxuan
     * @Description 通过所有枚举
     * @date date 2017-7-20
     */
    @GetMapping("/getEnumNames")
    @ApiOperation(value = "通过所有枚举", notes = "通过所有枚举")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "token", value = "token", required = true, dataType = "string", paramType = "query"),
    })
    public R getEnumNames() {
        List<EnumBean> data = new ArrayList<>();
        String folderPath = this.getClass().getResource("/").getPath() + Constant.PACKAGE_NAME.replace(".", "/");
        File folder = new File(folderPath);
        String[] fileNames = folder.list();
        if (fileNames != null) {
            for (String fileName : fileNames) {
                EnumBean enumBean = new EnumBean();
                enumBean.setValue(fileName.substring(0, fileName.length() - 6));
                enumBean.setLabel(fileName.substring(0, fileName.length() - 6));
                data.add(enumBean);
            }
        }
        return R.ok().put("data", data);
    }

    /**
     * @param
     * @author chenxuan
     * @Description 通过表码获取数据列表
     * @date date 2017-7-20
     */
    @GetMapping("/getDict")
    @ApiOperation(value = "通过表码获取数据列表", notes = "通过表码获取数据列表")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "dictName", value = "字典名称", required = true, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "token", value = "token", required = true, dataType = "string", paramType = "query"),
    })
    public R getCodeValues(String dictName) {
        Map<String, Object> params = new HashMap<>();
        params.put("dictName", dictName);
        List<EnumBean> data = sysDictService.getCodeValues(params);
        return R.ok().put("data", data);
    }

    @PostMapping("/upload")
    @ApiOperation(value = "上传文件", notes = "上传文件")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "token", value = "token", required = true, dataType = "string", paramType = "query"),
    })
    public R upload(@RequestParam("file") MultipartFile file) throws Exception {
        if (file.isEmpty()) {
            throw new MyException("上传文件不能为空");
        }

        return R.ok().put("url", "/static/img/head.jpg").put("fileName", file.getOriginalFilename());
    }

}
