package com.esionet.controller.admin;

import com.esionet.common.annotation.SysLog;
import com.esionet.common.api.annotation.RequireAuth;
import com.esionet.common.enumresource.StatusEnum;
import com.esionet.common.enumresource.WhetherEnum;
import com.esionet.common.utils.bean.Query;
import com.esionet.common.utils.bean.R;
import com.esionet.entity.SysDict;
import com.esionet.service.SysDictService;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.*;


/**
 * 后台管理-字典模块
 * @author chenxuan
 */
@RestController
@RequestMapping("/admin/dict")
@Api(tags = "后台管理-字典模块")
public class AdminDictController {

    @Autowired
    private SysDictService dictService;
    /**
     * 登录
     */


    @ApiOperation(value="获取字典列表", notes="获取字典列表")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "token", value = "token", required = true,dataType = "string",paramType = "query"),
            @ApiImplicitParam(name = "当前", value = "page", required = true,dataType = "integer",paramType = "query"),
            @ApiImplicitParam(name = "每页显示几条", value = "limit", required = true,dataType = "integer",paramType = "query"),
            @ApiImplicitParam(name = "字典名称", value = "dictCode", required = false,dataType = "string",paramType = "query"),
    })
    @GetMapping("list")
    @RequireAuth("dict:list")
    public R list(@RequestParam Map<String,Object> params) {
        //查询列表数据
         Query query = new Query(params);
         List<SysDict> list = dictService.getList(query);
         int total = dictService.getCount(query);
         return R.ok().put("data",list).put("total",total);
    }

    @ApiOperation(value="获取字典", notes="获取字典")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "token", value = "token", required = true,dataType = "string",paramType = "query"),
    })
    @GetMapping("getDictNames")
    public R getDictNames() {
        List<Map<String,Object>> list = dictService.getDictNames();
        return R.ok().put("data",list);
    }


    @ApiOperation(value="保存字典", notes="保存字典")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "token", value = "token", required = true,dataType = "string",paramType = "query"),
    })
    @SysLog("保存字典")
    @PostMapping("save")
    @RequireAuth("dict:save")
    public R save(@RequestBody @ApiParam(name="字典对象",value="json格式") SysDict sysDict) {
        sysDict.setCreateTime(new Date());
        sysDict.setIsSys(WhetherEnum.NO.getValue());
        sysDict.setStatus(StatusEnum.ENABLE.getValue());
        dictService.save(sysDict);
        return R.ok();
    }

    @ApiOperation(value="修改字典", notes="修改字典")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "token", value = "token", required = true,dataType = "string",paramType = "query"),
    })
    @SysLog("修改字典")
    @PostMapping("update")
    @RequireAuth("dict:update")
    public R update(@RequestBody @ApiParam(name="字典对象",value="json格式") SysDict sysDict) {
        sysDict.setUpdateTime(new Date());
        dictService.update(sysDict);
        return R.ok();
    }

    /**
     * 删除
     */
    @ApiOperation(value="删除字典", notes="删除字典")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "token", value = "token", required = true,dataType = "string",paramType = "query"),
    })
    @SysLog("删除字典")
    @DeleteMapping("value")
    @RequireAuth("dict:delete")
    public R delete(@RequestBody @ApiParam(name="要删除的ID数组",value="数组格式")String[] ids){
        dictService.deleteBatch(ids);
        return R.ok();
    }

}
