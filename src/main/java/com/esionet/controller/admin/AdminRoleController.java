package com.esionet.controller.admin;

import java.util.*;

import com.esionet.common.api.annotation.CurrentSysUser;
import com.esionet.common.api.annotation.RequireAuth;
import com.esionet.common.enumresource.StatusEnum;
import com.esionet.common.annotation.SysLog;
import com.esionet.common.enumresource.WhetherEnum;
import com.esionet.common.utils.bean.EnumBean;
import com.esionet.entity.SysUser;
import com.esionet.service.SysRoleMenuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import io.swagger.annotations.*;
import com.esionet.entity.SysRole;
import com.esionet.service.SysRoleService;
import com.esionet.common.utils.bean.Query;
import com.esionet.common.utils.bean.R;

/**
 * @Description: 后台管理-角色模块
 * @since chenxuan 2019-10-11 23:06:01
 */
@RestController
@RequestMapping("/admin/role")
@Api(tags = "后台管理-角色模块")
public class AdminRoleController {

    @Autowired
    private SysRoleService roleService;

    @Autowired
    private SysRoleMenuService sysRoleMenuService;


    /**
     * @Decription: 查询字典列表
     * @param: params 查询参数
     * @return: data列表数据 total列表总数
     * @since: chenxuan 2019-10-11 23:06:01
     **/
    @ApiOperation(value = "获取角色列表", notes = "获取角色列表")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "token", value = "token", required = true, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "当前", value = "page", required = true, dataType = "integer", paramType = "query"),
            @ApiImplicitParam(name = "每页显示几条", value = "limit", required = true, dataType = "integer", paramType = "query"),
    })
    @GetMapping("list")
    @RequireAuth("role:list")
    public R list(@RequestParam Map<String, Object> params) {
        //查询列表数据
        Query query = new Query(params);

        List<SysRole> roleList = roleService.getList(query);
        int total = roleService.getCount(query);
        return R.ok().put("data", roleList).put("total", total);
    }


    /**
     * 角色列表
     */
    @ApiOperation(value = "获取角色数据", notes = "用于前端组件数据封装")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "token", value = "token", required = true, dataType = "string", paramType = "query"),
    })
    @GetMapping("select")
    public R select() {
        Map<String, Object> map = new HashMap<>();
        List<SysRole> list = roleService.getList(map);
        List<EnumBean> values = new ArrayList<>();
        for (SysRole role : list) {
            EnumBean enumBean = new EnumBean();
            enumBean.setValue(role.getId());
            enumBean.setLabel(role.getRoleName());
            values.add(enumBean);
        }

        return R.ok().put("data", values);
    }


    /**
     * @Decription: 保存角色
     * @return: role 角色对象
     * @since: chenxuan 2019-10-11 23:06:01
     **/
    @ApiOperation(value = "保存角色", notes = "保存角色")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "token", value = "token", required = true, dataType = "string", paramType = "query"),
    })
    @SysLog("保存角色")
    @PostMapping("save")
    @RequireAuth("role:save")
    public R save(@RequestBody @ApiParam(name = "角色对象", value = "json格式") SysRole role, @CurrentSysUser SysUser user) {
        //角色编码不能重复
        Map<String, Object> params = new HashMap<>();
        params.put("roleCode", role.getRoleCode());
        int count = roleService.getCount(params);
        if (count > 0) {
            return R.error("该角色编码已存在!");
        }
        role.setCreateUserId(user.getUserId());
        role.setIsSys(WhetherEnum.NO.getValue());
        role.setStatus(StatusEnum.ENABLE.getValue());
        roleService.save(role);
        return R.ok();
    }

    /**
     * @Decription: 修改角色
     * @return: role 角色对象
     * @since: chenxuan 2019-10-11 23:06:01
     **/
    @ApiOperation(value = "修改角色", notes = "修改角色")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "token", value = "token", required = true, dataType = "string", paramType = "query"),
    })
    @SysLog("修改角色")
    @PostMapping("update")
    @RequireAuth("role:update")
    public R update(@RequestBody @ApiParam(name = "角色对象", value = "json格式") SysRole role, @CurrentSysUser SysUser user) {
        role.setUpdateTime(new Date());
        role.setCreateUserId(user.getUserId());
        roleService.update(role);
        return R.ok();
    }

    /**
     * @Decription: 删除角色
     * @return: ids 数组
     * @since: chenxuan 2019-10-11 23:06:01
     **/
    @ApiOperation(value = "删除角色", notes = "删除角色")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "token", value = "token", required = true, dataType = "string", paramType = "query"),
    })
    @SysLog("删除角色")
    @DeleteMapping("/delete")
    @RequireAuth("role:delete")
    public R delete(@RequestBody @ApiParam(name = "ids", value = "数组格式") String[] ids) {
        roleService.deleteBatch(ids);

        return R.ok();
    }


    /**
     * 角色信息
     */
    @ResponseBody
    @GetMapping("/getMenuIdList")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "token", value = "token", required = true, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "角色Id", value = "roleId", required = true, dataType = "string", paramType = "query"),
    })
    public R getMenuIdList(String roleId) {
        //查询角色对应的菜单
        List<String> menuIdList = sysRoleMenuService.getMenuIdList(roleId);
        return R.ok().put("data", menuIdList);
    }

}
